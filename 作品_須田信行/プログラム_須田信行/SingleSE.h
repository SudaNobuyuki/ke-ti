//インクルードガード
#pragma once
#ifndef SINGLE_SE_H
#define SINGLE_SE_H
#endif // !SINGLE_SE_H


//インクルード
#include <Windows.h>


//コメント
#pragma comment(lib,"winmm")


//単音(単体SE)を管理する名前空間
namespace SingleSE
{
	//音を鳴らす
	//引数:音源のパス
	//引数:再生フラグ
	//戻値:なし
	void PlaySoundEffect(LPCSTR soundPath, DWORD flag = SND_ASYNC);
};