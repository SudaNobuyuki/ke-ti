//インクルード
#include "Pause.h"
#include "PlayScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "SingleSE.h"


//名前空間
using namespace Input;


//定数
//Update()
static const float SIZE_MAGN = 0.3f;		//sinカーブの倍率
static const float SIZE_BASE = 0.9f;		//sinカーブの基礎値
static const int CHANGE_SIZE_TIME = 3;		//sinカーブの角度

//Draw()
static const float INIT_DRAW_POS = 1.2f;		//描画開始高さ
static const float NEXT_DRAW_POS = 0.6f;		//次の描画高さ

//SelectPauseChangeProcess()
static const int SELECT_PAUSE_CONTRACTION_CYCLE_MAXIMUM = 60;		//selectPauseContractionCycle_の最大値


//マクロ
//SelectPauseChangeProcess()
#define SELECT_PAUSE_CONTRACTION_CYCLE_SIN_CURVE (sinf(XMConvertToRadians(selectPauseContractionCycle_ * 3.0f)))		//selectPauseContractionCycle_のサインカーブ
#define SELECT_PAUSE_SCALING (abs(SELECT_PAUSE_CONTRACTION_CYCLE_SIN_CURVE) * 0.1f + 0.95f)							//selectPause_の画像を描画する際の拡大率(0.95倍〜1.05倍)



//コンストラクタ
Pause::Pause(GameObject * parent) : GameObject(parent, "Pause"),
	selectPause_(RETURN), pauseImageScale_(g_XMZero), selectPauseContractionCycle_(0),
	backgroundImageID_(INITIAL_ID),
	isPause_(false)
{
	//画像ID初期値
	for (int i = 0; i < PAUSE_IMAGES_MAXIMUM; i++)
		pauseImageID_[i] = INITIAL_ID;
}

//デストラクタ
Pause::~Pause()
{
}

//初期化
void Pause::Initialize()
{
	//各ポーズ画像の初期設定
	PausemageInitialSetting();

	//背景画像の初期設定
	BackgroundInitialSetting();
}

//更新
void Pause::Update()
{
	//ポーズ状態の切り替え
	PauseProcess();
	
	//選択しているポーズ画像を変更する処理
	SelectPauseChangeProcess();
}

//描画
void Pause::Draw()
{
	//ポーズ状態の場合の処理
	if (isPause_)
	{
		//背景の描画
		Image::Draw(backgroundImageID_);

		//各ポーズ画面の画像の描画
		for (int i = 0; i < PAUSE_IMAGES_MAXIMUM; i++)
			Image::Draw(pauseImageID_[i]);
	}
}

//開放
void Pause::Release()
{
}

//各ポーズ画像の初期設定
void Pause::PausemageInitialSetting()
{
	//各ポーズ画像データのパス
	std::string path[PAUSE_IMAGES_MAXIMUM] =
	{
		"Assets/PlayScene/Return.png",
		"Assets/PlayScene/Restart.png",
		"Assets/PlayScene/Reselect.png",
	};

	//各ポーズ画像のロードおよび拡大率をウィンドウサイズに調整
	for (int i = 0; i < PAUSE_IMAGES_MAXIMUM; i++)
	{
		//ポーズ画像データのロード
		pauseImageID_[i] = Image::Load(path[i]);
		assert(pauseImageID_[i] >= 0);

		//ポーズ画像の拡大率をウィンドウサイズに調整する
		pauseImageScale_ = Image::SetWindowSize(pauseImageID_[i]);
	}
}

//選択しているポーズ画像を変更する処理
void Pause::SelectPauseChangeProcess()
{
	//ポーズ中時にいずれか[Wキー押下・上キー押下・マウスホイールを上方向へ回す]を行った場合上へ移動させる処理
	if (isPause_ && KeyStateCheckDown(DIK_W, DIK_UP) || GetMouseMoveWheel() > 0)
	{
		//上へ移動させた場合にRETURNを下回ってしまった場合に一番下に移動させる処理
		if (--selectPause_ < RETURN)
			selectPause_ = RESELECT;

		//メニュー変更SEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/20210421141129.wav");
	}

	//ポーズ中時にいずれか[Sキー押下・下キー押下・マウスホイールを下方向へ回す]を行った場合下へ移動させる処理
	else if (isPause_ && KeyStateCheckDown(DIK_S, DIK_DOWN) || GetMouseMoveWheel() < 0)
	{
		//下へ移動させた場合にRESELECTを上回ってしまった場合に一番上に移動させる処理
		if (++selectPause_ > RESELECT)
			selectPause_ = RETURN;

		//メニュー変更SEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/20210421141129.wav");
	}

	//selectpauseContractionCycle_をインクリメントした場合に60を超えた場合に0に戻す処理
	if (++selectPauseContractionCycle_ >= SELECT_PAUSE_CONTRACTION_CYCLE_MAXIMUM)
		selectPauseContractionCycle_ = 0;

	//各ステージ画像の描画座標と拡大率を設定
	for (int pause = 0; pause < PAUSE_IMAGES_MAXIMUM; pause++)
	{
		//selectPause_の場合にはそのポーズ画像を拡縮する処理
		if (pause == selectPause_)
			transform_.scale_ *= SELECT_PAUSE_SCALING;

		//selectPause_以外の場合にはウィンドウサイズに設定する処理
		else
			transform_.scale_ = pauseImageScale_;

		//ステージ画像の表示位置と拡大率設定
		Image::SetPositionAndScale(pauseImageID_[pause], transform_);
	}
}

//背景画像の初期設定
void Pause::BackgroundInitialSetting()
{
	//背景画像データのロード
	backgroundImageID_ = Image::Load("Assets/PlayScene/Pause.png");
	assert(backgroundImageID_ >= 0);

	//背景画像の拡大率をウィンドウサイズに調整する
	Image::SetWindowSize(backgroundImageID_);
}

//ポーズ状態の切り替え
void Pause::PauseProcess()
{
	//ポーズ中では無い時にエンターキーを押下された場合の処理
	if (!isPause_ && IsKeyDown(DIK_RETURN))
	{
		//選択しているポーズ画像を一番上(ゲームに戻る)にする
		selectPause_ = RETURN;

		//ポーズ中にする
		isPause_ = true;

		//ポーズメニューポップアップを開くSEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/nc1281.wav");
	}

	//ポーズ中時にいずれか[スペースキー押下・エンターキー押下・マウスを左クリック]を行った場合の処理
	else if (isPause_ && (KeyStateCheckDown(DIK_SPACE, DIK_RETURN) || Input::IsMouseButtonDown(MOUSE_LEFT)))
	{
		//ポーズ中の処理
		switch (selectPause_)
		{
			//ゲームに戻る処理
		case RETURN:
			isPause_ = false;
			break;

			//ステージを最初から
		case RESTART:
			dynamic_cast<PlayScene*>(pParent_)->ReScene();
			break;

			//ステージセレクトに戻る
		case RESELECT:
			dynamic_cast<PlayScene*>(pParent_)->ChangeScene();
			break;
		}

		//ポーズメニューポップアップを閉じるSEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/nc1281.wav");
	}

	//プレイシーンにポーズの状態を伝える
	dynamic_cast<PlayScene*>(pParent_)->SetIsPause(isPause_);
}