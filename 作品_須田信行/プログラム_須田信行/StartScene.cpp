//インクルード
#include "StartScene.h"
#include "Splash.h"
#include "Title.h"


//定数
//Initialize()
static const float CLEAR_COLOR[RGB] = { 0, 0, 0 };		//このシーンの背景色
		

//コンストラクタ
StartScene::StartScene(GameObject * parent) : GameObject(parent, "StartScene")
{
}

//デストラクタ
StartScene::~StartScene()
{
}

//初期化
void StartScene::Initialize()
{
	//背景色変更
	Direct3D::SetClearColor(CLEAR_COLOR);

	//各オブジェクトのポインタの初期化
	ObjectsInitialize();

	///スプラッシュオブジェクトの生成
	SplashGeneration();
}

//更新
void StartScene::Update()
{
}

//描画
void StartScene::Draw()
{
}

//開放
void StartScene::Release()
{	
	//各オブジェクトのポインタの解放
	ObjectsRelease();
}

//各オブジェクトのポインタの解放
void StartScene::ObjectsRelease()
{
	//スプラッシュオブジェクト
	pSplash_ = nullptr;

	//タイトルオブジェクト
	pTitle_ = nullptr;
}

//スプラッシュオブジェクトの生成
void StartScene::SplashGeneration()
{
	//スプラッシュオブジェクトの生成
	pSplash_ = Instantiate<Splash>();

	//生成に成功したか
	assert(pSplash_ != nullptr);
}

//タイトルオブジェクトの生成
void StartScene::TitleGeneration()
{
	//タイトルオブジェクトの生成
	pTitle_ = Instantiate<Title>();

	//生成に成功したか
	assert(pTitle_ != nullptr);
}