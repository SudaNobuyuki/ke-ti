//インクルードガード
#pragma once
#ifndef PAUSE_H
#define PAUSE_H
#endif // !PAUSE_H


//インクルード
#include "Engine/GameObject.h"


//ポーズを管理するクラス
class Pause : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Pause(GameObject* parent);

	//デストラクタ
	//param :なし
	~Pause();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*ポーズ画面の画像管理*/

	//ポーズ画面の画像の種類
	enum PAUSE_IMAGES
	{
		RETURN,
		RESTART,
		RESELECT,
		PAUSE_IMAGES_MAXIMUM
	};

	//各ポーズ画像ID
	int pauseImageID_[PAUSE_IMAGES_MAXIMUM];

	//現在選択しているポーズ画面の画像
	int selectPause_;

	//ポーズ画像の拡大率をウィンドウサイズに合わせた際の大きさを記憶
	XMVECTOR pauseImageScale_;

	//選択しているポーズ画像の拡縮周期
	int selectPauseContractionCycle_;

	//各ポーズ画像の初期設定
	//param :なし
	//return:なし
	void PausemageInitialSetting();

	//選択しているポーズ画像を変更する処理
	//param :なし
	//return:なし
	void SelectPauseChangeProcess();

private:
	/*背景画像管理*/

	//背景画像ID
	int backgroundImageID_;

	//背景画像の初期設定
	//param :なし
	//return:なし
	void BackgroundInitialSetting();

private:
	/*ポーズ中の動作を管理*/

	//ポーズ中か
	bool isPause_;

	//ポーズ状態の切り替え及びポーズ中の処理
	//param :なし
	//return:なし
	void PauseProcess();

public:
	/*今ポーズ中かを知る*/

	//ポーズ中かどうかを返す
	//param :なし
	//return:ポーズ中か
	bool IsPause() { return isPause_; }
};