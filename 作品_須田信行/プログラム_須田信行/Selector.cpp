//インクルード
#include "Selector.h"
#include "StageConfig.h"
#include "StageSelectScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "SingleSE.h"


//名前空間
using namespace StageConfig;
using namespace Input;


//定数
//StageImageSetPositionAndScale()
static const int SELECT_STAGE_CONTRACTION_CYCLE_MAXIMUM = 60;		//selectStageContractionCycle_の最大値

//SelectStageChangeProcess()
static const int INITIAL_VALUE_OF_SELECT_STAGE_MOVE_TIME_ = 50;		//selectStageMoveTime_が設定される際の初期値
static const float SELECT_STAGE_THAT_CHANGE_WHEN_SPEED = 0.02f;		//selectStage_を変更する際の移動速度


//マクロ
//StageImageSetPositionAndScale()
#define SELECT_STAGE_CONTRACTION_CYCLE_SIN_CURVE (sinf(XMConvertToRadians(selectStageContractionCycle_ * 3.0f)))		//selectStageContractionCycle_のサインカーブ
#define SELECT_STAGE_POSITION_Y (-SELECT_STAGE_CONTRACTION_CYCLE_SIN_CURVE * 0.03f)										//selectStage_の画像を描画する際のY座標
#define SELECT_STAGE_SCALING (abs(SELECT_STAGE_CONTRACTION_CYCLE_SIN_CURVE) * 0.1f + 0.95f)								//selectStage_の画像を描画する際の拡大率(0.95倍〜1.05倍)

//SelectStageChangeProcess()
#define MAX_STAGE (STAGE_MAXIMUM - 1)


//コンストラクタ
Selector::Selector(GameObject * parent) : GameObject(parent, "Selector"),
	selectStage_(STAGE_1), previousSelectStage_(STAGE_1), selectStageMoveDirection_(0), selectStageMoveTime_(0), selectStageMovePosition_(0), selectStageContractionCycle_(0),
	stageImageScale_(g_XMZero)
{
	//各ステージ画像IDおよび各ステージの説明画像IDの初期化
	for (int i = 0; i < STAGE_MAXIMUM; i++)
	{
		//ステージ画像IDの初期化
		stageImageID_[i] = INITIAL_ID;

		//ステージの説明画像IDの初期化
		stageDescriptionImageID_[i] = INITIAL_ID;
	}
}

//デストラクタ
Selector::~Selector()
{
}

//初期化
void Selector::Initialize()
{
	//各ステージ画像の初期設定
	StageImageInitialSetting();
	
	//各ステージの説明画像の初期設定
	StageDescriptionImageInitialSetting();
}

//更新
void Selector::Update()
{
	//選択しているステージを変更する処理
	SelectStageChangeProcess();

	//各ステージ画像の描画座標と拡大率を設定する処理
	StageImageSetPositionAndScale();

	//プレイシーンへ遷移する処理
	NextScene();
}

//描画
void Selector::Draw()
{
	//各ステージ画像の描画
	for (int stage = 0; stage < STAGE_MAXIMUM; stage++)
		Image::Draw(stageImageID_[stage]);

	//選択しているステージの説明画像の描画
	Image::Draw(stageDescriptionImageID_[selectStage_]);
}

//開放
void Selector::Release()
{
}

//各ステージ画像の初期設定
void Selector::StageImageInitialSetting()
{
	//各ステージ画像のロードおよび拡大率をウィンドウサイズに調整
	for (int i = 0; i < STAGE_MAXIMUM; i++)
	{
		//ステージ画像データのロード
		stageImageID_[i] = Image::Load(GetStageImagePath(i));
		assert(stageImageID_[i] >= 0);
	
		//ステージ画像の拡大率をウィンドウサイズに調整
		Image::SetWindowSize(stageImageID_[i]);
	}
}

//各ステージ画像の描画座標と拡大率を設定する処理
void Selector::StageImageSetPositionAndScale()
{
	//selectStageContractionCycle_をインクリメントした場合に60を超えた場合に0に戻す処理
	if (++selectStageContractionCycle_ >= SELECT_STAGE_CONTRACTION_CYCLE_MAXIMUM)
		selectStageContractionCycle_ = 0;

	//各ポーズ画像の描画座標と拡大率を設定
	for (int stage = 0; stage < STAGE_MAXIMUM; stage++)
	{
		//ステージ画像を描画する際のX座標の設定
		transform_.position_.vecX = stage - previousSelectStage_ + selectStageMovePosition_;

		//selectStage_の場合はそのステージ画像の拡縮とそれに伴う上下移動の移動を処理
		if (stage == selectStage_)
		{
			//ステージ画像を描画する際のY座標の設定
			transform_.position_.vecY = SELECT_STAGE_POSITION_Y;

			//ステージ画像を描画する際の拡大率設定
			transform_.scale_ *= SELECT_STAGE_SCALING;
		}

		//selectStage_以外の場合の処理
		else
		{
			//ステージ画像を描画する際のY座標を0に設定
			transform_.position_.vecY = 0;

			//ステージ画像を描画する際の拡大率をウィンドウサイズに設定
			transform_.scale_ = stageImageScale_;
		}

		//ステージ画像の表示位置と拡大率設定
		Image::SetPositionAndScale(stageImageID_[stage], transform_);
	}
}

//各ステージの説明画像の初期設定
void Selector::StageDescriptionImageInitialSetting()
{
	//各ステージの説明画像のロードおよび拡大率をウィンドウサイズに調整
	for (int i = 0; i < STAGE_MAXIMUM; i++)
	{
		//ステージの説明画像データのロード
		stageDescriptionImageID_[i] = Image::Load(GetStageDescriptionImagePath(i));
		assert(stageDescriptionImageID_[i] >= 0);

		//ステージの説明画像の拡大率をウィンドウサイズに調整
		stageImageScale_ = Image::SetWindowSize(stageDescriptionImageID_[i]);
	}
}

//選択しているステージを変更する処理
void Selector::SelectStageChangeProcess()
{
	//選択しているステージが変更中ではない場合の処理
	if (selectStage_ == previousSelectStage_)
	{
		//右へ移動させる処理(現在一番右ではない状態でいずれか[Dキー押下・右キー押下・マウスホイールを下方向へ回す]を行った場合)
		if (selectStage_ < MAX_STAGE && (KeyStateCheck(DIK_D, DIK_RIGHT) || GetMouseMoveWheel() < 0))
			selectStage_++;

		//左へ移動させる処理(現在一番左ではない状態でいずれか[Aキー押下・左キー押下・マウスホイールを上方向へ回す]を行った場合)
		if (selectStage_ > STAGE_1 && (KeyStateCheck(DIK_A, DIK_LEFT) || GetMouseMoveWheel() > 0))
			selectStage_--;

		//上記どちらかを実行した場合の処理
		if (selectStage_ != previousSelectStage_)
		{
			//ステージ変更SEを流す
			SingleSE::PlaySoundEffect("Assets/Sound/20210422160809.wav");

			//選択しているステージを変更させる時間を設定する
			selectStageMoveTime_ = INITIAL_VALUE_OF_SELECT_STAGE_MOVE_TIME_;
		}

		//今のステージを伝える
		StageConfig::selectStage_ = (STAGES)selectStage_;
	}

	//選択しているステージが変更中の場合の処理
	else
	{
		//選択しているステージを変更する際に見た目で移動している様に処理する際のX座標ポジション設定
		selectStageMovePosition_ += (previousSelectStage_ - selectStage_) * SELECT_STAGE_THAT_CHANGE_WHEN_SPEED;

		//selectStageMoveTime_をデクリメントした場合に50フレームを過ぎていた場合の処理
		if (--selectStageMoveTime_ < 0)
		{
			//前に選択したステージを更新
			previousSelectStage_ = selectStage_;

			//選択しているステージを変更する際に見た目で移動している様に処理する際のX座標ポジションを0に戻す
			selectStageMovePosition_ = 0;
		}
	}
}

//プレイシーンへ遷移する処理
void Selector::NextScene()
{
	//プレイシーンへ遷移する処理(いずれか[スペースキー押下・エンターキー押下・マウスを左クリック]を行った場合)
	if (Input::KeyStateCheckDown(DIK_SPACE, DIK_RETURN) || Input::IsMouseButtonDown(MOUSE_LEFT))
	{
		//ステージ決定SEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/nc145056.wav");

		//シーンを移動
		dynamic_cast<StageSelectScene*>(pParent_)->NextScene();
	}
}