//インクルードガード
#pragma once
#ifndef TIMER_H
#define TIMER_H
#endif // !TIMER_H


//インクルード
#include "Engine/GameObject.h"


//時間を管理するクラス
class Timer : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Timer(GameObject* parent);

	//デストラクタ
	//param :なし
	~Timer();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*数字(時間)画像管理*/

	//数字(時間)の画像の種類
	enum TIME_IMAGES
	{
		ZERO,
		ONE,
		TWO,
		THREE,
		FORE,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		MINUTE,
		SECOND,
		TIME_IMAGE_MAXIMUM
	};

	//時間画像ID
	int timeImageID_[TIME_IMAGE_MAXIMUM];

	//フレーム単位で数えた総時間(数字の切り替え用)
	int totalTime_;

	//各時間画像の初期設定
	//param :なし
	//return:なし
	void TimeImageInitialSetting();

	//時間の描画処理
	//param :なし
	//return:なし
	void TimeImageDrawProcess();

	//時間の描画
	//param :時間
	//return:なし
	void TimeDraw(int time);

private:
	/*時間の記録関連*/

	//記録の描画開始タイミング
	int startRecordDrawTime_;

	//過去最高記録時間
	int recordTime_;

	//過去最高記録時間の取得
	//param :なし
	//return:なし
	void GetRecordTime();

	//時間を記録する場合は記録する
	//param :なし
	//return:なし
	void SaveTime();

public:
	/*NewRecordか*/

	//NewRecordか
	//param :なし
	//return:過去最高記録より今回の記録のほうが上回っているか
	bool IsNewRecord();
};