//インクルード
#include "First.h"
#include "PlayScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//定数
//Update()
static const int DRAW_IMAGE_ADVANCE_SPEED = 2;		//キー入力の重み
static const int TIMING_TO_PROCEED_TO_PLAY = 3;		//プレイ画面に移るタイミング

//Draw()
static const float DRAWING_SWITCHING_SPEED = 0.1f;		//描画切り替えタイミング


//コンストラクタ
First::First(GameObject* parent) : GameObject(parent, "First"),
drawImage_(0), drawTime_(0)
{
	//各操作方法画像の初期化
	for (int i = 0; i < HOW_TO_PLAY_IMAGE_MAXIMUM; i++)
		howToPlayImageID_[i] = INITIAL_ID;
}

//デストラクタ
First::~First()
{
	//プレイシーン内の自身以外のオブジェクト生成
	dynamic_cast<PlayScene*>(pParent_)->PlaySceneInstantiate();
}

//初期化
void First::Initialize()
{
	//各操作方法画像の初期設定
	HowToPlayImageInitialSetting();
}

//更新
void First::Update()
{
	//操作方法の切り替えまたはプレイ画面へ遷移する処理(いずれか[スペースキー押下・エンターキー押下・マウスを左クリック]を行った場合)
	if ((drawImage_ += (Input::KeyStateCheckDown(DIK_SPACE, DIK_RETURN) | Input::IsMouseButtonDown(MOUSE_LEFT)) * DRAW_IMAGE_ADVANCE_SPEED) > TIMING_TO_PROCEED_TO_PLAY)
		KillMe();
}

//描画
void First::Draw()
{
	//操作方法画像の描画
	Image::Draw(howToPlayImageID_[drawImage_ + (int)round(abs(sin(drawTime_++ * DRAWING_SWITCHING_SPEED)))]);
}

//開放
void First::Release()
{
}

//各操作方法画像の初期設定
void First::HowToPlayImageInitialSetting()
{
	//各操作方法画像データのパス
	std::string howToPlayImagePath[HOW_TO_PLAY_IMAGE_MAXIMUM] =
	{
		"Assets/PlayScene/HowToPlay1.png",
		"Assets/PlayScene/HowToPlay2.png",
		"Assets/PlayScene/HowToPlay3.png",
		"Assets/PlayScene/HowToPlay4.png",
	};

	//各操作方法画像のロードおよび拡大率をウィンドウサイズに調整
	for (int i = 0; i < HOW_TO_PLAY_IMAGE_MAXIMUM; i++)
	{
		//時間画像データのロード
		howToPlayImageID_[i] = Image::Load(howToPlayImagePath[i]);
		assert(howToPlayImageID_[i] >= 0);

		//時間画像の拡大率をウィンドウサイズに調整
		Image::SetWindowSize(howToPlayImageID_[i]);
	}
}