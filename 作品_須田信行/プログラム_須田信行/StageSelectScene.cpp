//インクルード
#include "StageSelectScene.h"
#include "Selector.h"


//定数
//Initioalize()
static const float CLEAR_COLOR[RGB] = { 0.2f, 0.4f, 0.8f};		//このシーンの背景色


//コンストラクタ
StageSelectScene::StageSelectScene(GameObject * parent) : GameObject(parent, "StageSelectScene")
{
}

//デストラクタ
StageSelectScene::~StageSelectScene()
{
}

//初期化
void StageSelectScene::Initialize()
{
	//背景色変更
	Direct3D::SetClearColor(CLEAR_COLOR);

	//各オブジェクトのポインタの初期化
	ObjectsInitialize();

	//ステージセレクトオブジェクトの生成
	SelectorGeneration();
}

//更新
void StageSelectScene::Update()
{
}

//描画
void StageSelectScene::Draw()
{
}

//開放
void StageSelectScene::Release()
{
	//各オブジェクトのポインタの解放
	ObjectsRelease();
}

//各オブジェクトのポインタの解放
void StageSelectScene::ObjectsRelease()
{
	//ステージセレクトオブジェクト
	pSelector = nullptr;
}

//ステージセレクトオブジェクトの生成
void StageSelectScene::SelectorGeneration()
{
	//スプラッシュオブジェクトの生成
	pSelector = Instantiate<Selector>();

	//生成に成功したか
	assert(pSelector != nullptr);
}