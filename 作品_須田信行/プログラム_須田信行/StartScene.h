//インクルードガード
#pragma once
#ifndef START_H
#define START_H
#endif // !START_H


//インクルード
#include "Engine/GameObject.h"
#include "Engine/SceneManager.h"


//スタートシーン(タイトルシーン)を管理するクラス
class StartScene : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	StartScene(GameObject* parent);

	//デストラクタ
	//param :なし
	~StartScene();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*自身を親とする各オブジェクトのポインタ*/

	//スプラッシュオブジェクトポインタ
	GameObject* pSplash_;

	//タイトルオブジェクトポインタ
	GameObject* pTitle_;

	//各オブジェクトのポインタの初期化(解放と共通)
	//param :なし
	//return:なし
	void ObjectsInitialize() { ObjectsRelease(); };

	//各オブジェクトのポインタの解放
	//param :なし
	//return:なし
	void ObjectsRelease();

public:
	/*自身を親とする各オブジェクトの生成関数*/

	//スプラッシュオブジェクトの生成
	//param :なし
	//return:なし
	void SplashGeneration();

	//タイトルオブジェクトの生成
	//param :なし
	//return:なし
	void TitleGeneration();

public:
	/*シーンを変更*/

	//シーンマネージャーオブジェクトにシーン切り替えを要請
	//param :なし
	//return:なし
	void NextScene() { dynamic_cast<SceneManager*>(pParent_)->NextScene(); }
};
