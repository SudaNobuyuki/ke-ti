﻿//インクルード
#include <list>
#include <stdlib.h>
#include <Windows.h>
#include "Engine/Direct3D.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Model.h"
#include "Engine/SceneManager.h"


//コメント
#pragma comment(lib, "winmm.lib")


//定数
static const char* WINDOW_CLASS_NAME = "Hopping Action";		//ウィンドウクラス名
static const int WINDOW_WIDTH = 1000;							//ウィンドウの幅
static const int WINDOW_HEIGHT = 700;							//ウィンドウの高さ
static const int FEED_SPD = 1000;								//1秒
static const int ONE_FLAME = 60;								//1フレーム

//ウィンドウサイズ用変数宣言
static int windowWidth = 0;
static int windowHeight = 0;

//マウスカーソル設定
static bool isSetMouseCursorFlg = false;
static bool isMouseCursorFlg = false;

//SceneManagerポインタ
static SceneManager* pSceneManager = nullptr;

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
void SetMouseCursor(HWND hWnd);

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	//ウィンドウクラス(設計図)を作成
	WNDCLASSEX wndclassex;
	wndclassex.cbSize = sizeof(WNDCLASSEX);					//この構造体のサイズ
	wndclassex.hInstance = hInstance;						//インスタンスハンドル
	wndclassex.lpszClassName = WINDOW_CLASS_NAME;			//ウィンドウクラス名
	wndclassex.lpfnWndProc = WndProc;						//ウィンドウプロシージャ
	wndclassex.style = CS_VREDRAW | CS_HREDRAW;				//スタイル(デフォルト)
	wndclassex.hIcon = LoadIcon(NULL, IDI_APPLICATION);		//アイコン
	wndclassex.hIconSm = LoadIcon(NULL, IDI_WINLOGO);		//小さいアイコン
	wndclassex.hCursor = LoadCursor(NULL, IDC_ARROW);		//マウスカーソル
	wndclassex.lpszMenuName = NULL;							//メニュー
	wndclassex.cbClsExtra = 0;								//追加領域
	wndclassex.cbWndExtra = 0;								//追加領域
	wndclassex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); //背景(白)
	RegisterClassEx(&wndclassex);

	//ウィンドウサイズの計算
	RECT windowRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, TRUE);
	windowWidth = windowRect.right - windowRect.left;			//ウィンドウ幅
	windowHeight = windowRect.bottom - windowRect.top;		//ウィンドウ高さ

	//プライマリーモニターの解像度取得
	RECT primaryMonitor;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &primaryMonitor, 0);

	//ウィンドウの表示位置
	RECT createPosition;
	SetRect(&createPosition, (primaryMonitor.right - windowWidth) / 2, (primaryMonitor.bottom - windowHeight) / 2, windowWidth, windowHeight);

	//ウィンドウを作成
	HWND hwnd = CreateWindow(
		WINDOW_CLASS_NAME,											//ウィンドウクラス名
		WINDOW_CLASS_NAME,											//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME & ~WS_MAXIMIZEBOX,		//スタイル(拡縮不可)
		createPosition.left,										//表示位置左
		createPosition.top,											//表示位置上
		createPosition.right,										//ウィンドウ幅
		createPosition.bottom,										//ウィンドウ高さ
		NULL,														//親ウインドウ(なし)
		NULL,														//メニュー(なし)
		hInstance,													//インスタンス
		NULL														//パラメータ(なし)
	);

	//ウィンドウを表示
	ShowWindow(hwnd, nCmdShow);

	//Direct3D::初期化
	if (FAILED(Direct3D::Initialize(WINDOW_WIDTH, WINDOW_HEIGHT, hwnd)))
		return 0;

	//DirectInputの初期化
	if (FAILED(Input::Initialize(hwnd)))
		return 0;

	//pSceneManagerの生成
	pSceneManager = new SceneManager;

	//pSceneManagerの初期化
	pSceneManager->Initialize();

	//メッセージループ(何か起きるのを待つ)
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			//メッセージの生成
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//メッセージなし
		else
		{
			//正確な時刻を刻む
			timeBeginPeriod(1);

			//更新回数記憶
			static DWORD countFps = 0;

			//アプリ起動時の時間
			static DWORD startTime = timeGetTime();

			//時間計測
			DWORD nowTime = timeGetTime();

			//最後の更新時間
			static DWORD lastUpdateTime = nowTime;

			//時間によるリセット(1秒)
			if (nowTime - startTime >= FEED_SPD)
			{
				//数字を文字に置き換える(1秒単位)
				char string[16];
				wsprintf(string, "%u", countFps);		//更新回数を文字に置き換える

				//初期化
				countFps = 0;
				startTime = nowTime;
			}

			//まだ1000/60秒立っていなければ画面をまだ更新しない
			//割り切れないを消すために両辺に60を掛ける
			if ((nowTime - lastUpdateTime) * ONE_FLAME <= FEED_SPD)
				continue;

			//画面の更新履歴
			lastUpdateTime = nowTime;

			//更新回数を増やす
			countFps++;

			//Input::更新
			if (FAILED(Input::Update()))
				return 0;

			//マウスカーソル設定
			SetMouseCursor(hwnd);

			//pSceneManager以下の更新
			pSceneManager->UpdateSub();

			//Direct3D::描画開始
			Direct3D::BeginDraw();

			//pSceneManager以下の描画
			pSceneManager->DrawSub();

			//Direct3D::描画終了
			if (FAILED(Direct3D::EndDraw()))
				return 0;

			//一時停止(1ms)
			Sleep(1);

			//正確な時間終了
			timeEndPeriod(1);
		}
	}

	//モデル開放
	Model::Release();

	//画像開放
	Image::Release();

	//pSceneManagerの削除
	SAFE_DELETE(pSceneManager);

	//Input::解放
	Input::Release();

	//Direct3D::解放
	Direct3D::Release();

	return 0;
}

//ウィンドウプロシージャ(何かあった時によばれる関数)
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//メッセージ
	switch (msg)
	{
		//ウィンドウのサイズが変更されたときクライアント領域を再設定
	case WM_SIZE:
		Direct3D::screenWidth_ = lParam & 0xFFFF;				//xサイズ
		Direct3D::screenHeight_ = (lParam >> 16) & 0xFFFF;		//yサイズ
		return false;

		//マウスの動き検知
	case WM_MOUSEMOVE:
		int posX, posY;
		posX = LOWORD(lParam), posY = HIWORD(lParam);
		Input::SetMousePosition(posX, posY);
		return false;

		//ウィンドウの削除
	case WM_DESTROY:
		//終了処理
		PostQuitMessage(0);
		return false;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//マウスカーソル設定
void SetMouseCursor(HWND hWnd)
{
	//キー入力で以下ソース実行フラグ切り替え
	if (Input::IsKeyDown(DIK_Q))
	{
		//フラグ切り替え
		isSetMouseCursorFlg = !isSetMouseCursorFlg;

		//falseに切り替えた時
		if (isSetMouseCursorFlg == false)
		{
			//マウスカーソルを表示させる
			isMouseCursorFlg = true;
			ShowCursor(isMouseCursorFlg);
		}
	}

	//マウスカーソル設定
	if (isSetMouseCursorFlg)
	{
		//マウスカーソル非表示設定
		if (Input::IsKeyDown(DIK_H))
		{
			ShowCursor(isMouseCursorFlg);
			isMouseCursorFlg = !isMouseCursorFlg;
		}

		//マウスカーソル表示位置設定
		if (isMouseCursorFlg)
		{
			//ウィンドウサイズ
			RECT rect;

			//ウィンドウの位置取得
			GetWindowRect(hWnd, &rect);

			//マウスカーソルの表示位置
			SetCursorPos((rect.right - rect.left) / 2 + rect.left, (rect.bottom - rect.top) / 2 + rect.top);
		}
	}
}