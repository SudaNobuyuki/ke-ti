//インクルード
#include "Player.h"
#include "StageConfig.h"
#include "PlayScene.h"
#include "Engine/Camera.h"
#include "Engine/CsvReader.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Model.h"
#include "SingleSE.h"


//名前空間
using namespace Input;


//定数
//コンストラクタ, CenterImageSettingProcess()
static const int INITIAL_VALUE_OF_CENTER_IMAGE_CHANGE_COUNT = 300;		//centerImageChangeCount_が設定される際の初期値

//CenterImageSettingProcess()
static const int SECOND = 60;			//1秒当たりのフレーム数
static const int GO_FLAME = 120;		//Goが表示を開始するフレーム

//PlayerInitialSetting()
static const float PLAYER_START_ANGLE[4] = { 0, 90, 180, 270 };		//プレイヤーのスタート角度(一覧)

//PlayerMoveProcess()
static const float AIR_RESISTANCE = 0.9f;					//プレイヤーが空中にいる際の減衰率(空気抵抗)
static const float REDUCED_MOVEMENT_IN_THE_AIR = 0.32f;		//プレイヤーが空中にいる際の入力による移動量の減衰率
static const float PLAYER_XZ_FAST_MOVE_SPEED = 0.12f;		//プレイヤーの速いXZ移動速度
static const float PLAYER_XZ_SLOWLY_MOVE_SPEED = 0.04f;		//プレイヤーの遅いXZ移動速度
static const float PLAYER_XZ_NORMAL_MOVE_SPEED = 0.08f;		//プレイヤーの通常XZ移動速度
static const int AXIS_X = 0;								//軸X
static const int AXIS_Z = 2;								//軸Z

//PlayerJumpProcess()
static const float GRAVITY = 0.006f;				//重力
static const float INITIAL_VELOCITY = 0.117f;		//ジャンプ時の初速

//ReturnToTheWallProcess(int axis)
static const int NUMBER_OF_COUNTER_ELECTRODE_SURFACES = 2;		//対極な面の数(左面:右面 / 下面:上面 / 正面:奥面)
static const int LEFT_OR_BOTTOM_OR_FRONT = 0;					//左面か下面か正面
static const int RIGHT_OR_TOP_OR_BACK = 1;						//右面か上面か奥面
static const float PLAYER_HEIGHT = 1.3f;						//プレイヤーの伸長
static const float PLAYER_WIDTH = 0.3f;							//プレイヤーの横幅
static const float PLAYER_HEIGHT_HARF = PLAYER_HEIGHT / 2;		//プレイヤーの伸長の半分
static const float ADJUSTMENT_PLAYER_HEIGHT = 0.1f;				//プレイヤーの上下面以外での当たり判定の計算の際に用いる天井すれすれを回避用の値

//PlayerJumpProcess(), ReturnToTheWallProcess(int axis)
static const float WALL_SIDE_HARF = 0.5f;		//壁オブジェクトの中心から面までの距離

//CameraMoveProcess()
static const float MOUSE_CURSOR_HORIZONTAL_MOVE_SPEED = 0.1f;				//マウスのX軸移動の速さ
static const float MOUSE_CURSOR_VERTICAL_MOVE_SPEED = 0.01f;				//マウスのY軸移動の速さ
static const XMVECTOR CAMERA_TARGET_POSITION = XMVectorSet(0, 0, 5, 0);		//カメラの焦点

//ReturnToTheWallProcess(int axis), CameraMoveProcess()
static const float CAMERA_HEIGHT = 0.8f;		//カメラの高さ


//マクロ
//PlayerInitialSetting()
#define GET_START_X ((float)stageConfig.GetValue(0, 1))		//プレイヤーのスタート位置X取得
#define GET_START_Y ((float)stageConfig.GetValue(1, 1))		//プレイヤーのスタート位置Y取得
#define GET_START_Z ((float)stageConfig.GetValue(2, 1))		//プレイヤーのスタート位置Z取得
#define GET_DIRECTION (stageConfig.GetValue(3, 1))			//プレイヤーのスタート角度取得

//ReturnToTheWallProcess()
#define IS_WALL(p) (dynamic_cast<PlayScene*>(pParent_)->IsWall(p))							//壁かどうか
#define IS_WALL_LOW_OR_MIDDLE_OR_HIGH(p, q, r) (IS_WALL(p) + IS_WALL(q) + IS_WALL(r))		//プレイヤーの移動先座標の内足元、身長の中心、頭のいずれかが壁と接触しているかどうか


//コンストラクタ
Player::Player(GameObject * parent) : GameObject(parent, "Player"),
	centerImage_(THREE), centerImageChangeCycle_(INITIAL_VALUE_OF_CENTER_IMAGE_CHANGE_COUNT),
	playerAngleYMatrix_(XMMatrixIdentity()),
	isPause_(false), isCentralImageDrawingAndItsCount_(true), isPlayerMove_(false), playerMoveState_(NORMAL), isMouseCursorDisplayed_(0), isMouseControl_(true),
	isPlayerOperationRelateOptionOnceActivation_(false), movingDistanceByEntry_(g_XMZero), playerMovingDistance_(g_XMZero), isPlayerInTheAir_(false), playerJumpAmount_(0),
	cameraHeight_(CAMERA_HEIGHT)
{
	//各中央に表示される画像IDの初期化
	for (int i = 0; i < CENTER_IMAGE_MAXIMUM; i++)
		centerImageID_[i] = INITIAL_ID;
}

//デストラクタ
Player::~Player()
{
	//マウスが非表示の場合表示させる処理
	if(isMouseCursorDisplayed_ < 0)
		ShowCursor(true);
}

//初期化
void Player::Initialize()
{
	//各中央に表示する画像の初期設定
	centerImageInitialSetting();

	//プレイヤーの初期設定
	PlayerInitialSetting();

	//マウスを非表示にする
	isMouseCursorDisplayed_ = ShowCursor(signbit((float)isMouseCursorDisplayed_));
}

//更新
void Player::Update()
{
	//プレイヤーがゴールしたか
	IsGoal();

	//中央に表示する画像を設定する処理
	CenterImageSettingProcess();

	//ポーズ状態が切り替わった場合の処理
	if(isPause_ != dynamic_cast<PlayScene*>(pParent_)->IsPause())
		PauseProcess();

	//ベクトルを回転させる際に使用する行列をプレイヤーの向いている方向から生成
	playerAngleYMatrix_ = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

	//プレイヤーの移動の処理
	PlayerMoveProcess();

	//マウスの操作設定の処理
	MouseControlProcess();

	//カメラの移動処理
	CameraMoveProcess();
}

//描画
void Player::Draw()
{
	//中央の画像の描画およびそのカウントが有効な場合中央に表示する画像描画処理
	if(isCentralImageDrawingAndItsCount_)
		Image::Draw(centerImageID_[centerImage_]);
}

//開放
void Player::Release()
{
}

//各中央に表示する画像の初期設定
void Player::centerImageInitialSetting()
{
	//各中央に表示する画像データのパス
	std::string centerImagePath[CENTER_IMAGE_MAXIMUM] =
	{
		"Assets/PlayScene/Center.png",
		"Assets/Numbers/Go.png",
		"Assets/Numbers/1.png",
		"Assets/Numbers/2.png",
		"Assets/Numbers/3.png",
	};

	//各中央に表示する画像のロードおよび拡大率をウィンドウサイズに調整
	for (int i = 0; i < CENTER_IMAGE_MAXIMUM; i++)
	{
		//ステージの説明画像データのロード
		centerImageID_[i] = Image::Load(centerImagePath[i]);
		assert(centerImageID_[i] >= 0);

		//ステージの説明画像の拡大率をウィンドウサイズに調整
		Image::SetWindowSize(centerImageID_[i]);
	}
}

//中央に表示する画像を設定する処理
void Player::CenterImageSettingProcess()
{
	//カウントを進める前の場合カウントを進めるSEを流す処理
	if(centerImageChangeCycle_ == INITIAL_VALUE_OF_CENTER_IMAGE_CHANGE_COUNT)
		SingleSE::PlaySoundEffect("Assets/Sound/nc10485.wav");

	//中央の画像の描画およびそのカウントが有効かつ中央に表示する画像を変更するタイミングのカウントを減らせる場合減らす
	if (isCentralImageDrawingAndItsCount_ && centerImageChangeCycle_ > 0)
		centerImageChangeCycle_--;

	//カウントが進んだ場合にカウントを進めるSEを流す処理
	if(centerImageChangeCycle_ / SECOND != centerImage_ && centerImage_ > ONE)
		SingleSE::PlaySoundEffect("Assets/Sound/nc10485.wav");

	//中央に表示する画像を変更
	centerImage_ = centerImageChangeCycle_ / SECOND;

	//中央に表示する画像を変更するタイミングのカウントがGO(120)を下回った場合に一度だけ処理する処理
	if (centerImageChangeCycle_ < GO_FLAME)
		PlayerOperationRelateOptionOnceActivateProcess();
}

//プレイヤーの初期設定
void Player::PlayerInitialSetting()
{
	//プレイヤーの初期情報が格納されているデータをロード
	CsvReader stageConfig;
	stageConfig.Load(StageConfig::GetStageDataConfigPath());

	//プレイヤーの座標を取得
	transform_.position_ = XMVectorSet(GET_START_X, GET_START_Y, GET_START_Z, 0);

	//プレイヤーの角度を取得
	transform_.rotate_.vecY = PLAYER_START_ANGLE[GET_DIRECTION];
}

//プレイヤーがゴールしたか
void Player::IsGoal()
{
	//プレイヤーの座標に壁までの距離と重力を与えた場合の座標
	XMVECTOR destinationCoordinates = transform_.position_ + XMVectorSet(WALL_SIDE_HARF, -GRAVITY, WALL_SIDE_HARF, 0);

	//空中に存在せずゴールの足場に触れた場合の処理
	if (!isPlayerInTheAir_ && dynamic_cast<PlayScene*>(pParent_)->IsGoal(destinationCoordinates))
	{
		isMouseCursorDisplayed_ = ShowCursor(true);
		dynamic_cast<PlayScene*>(pParent_)->KillPause();
		dynamic_cast<PlayScene*>(pParent_)->KillPlayer();
	}
}

//一度のみプレイヤーの操作(キーボード)各種設定を有効にする処理
void Player::PlayerOperationRelateOptionOnceActivateProcess()
{
	//一度のみ実行
	if (isPlayerOperationRelateOptionOnceActivation_ == false)
	{
		//プレイヤーの移動を有効
		isPlayerMove_ = !isPlayerMove_;

		//タイマーを生成
		dynamic_cast<PlayScene*>(pParent_)->TimerGeneration();

		//一度のみプレイヤーの操作(キーボード)各種設定を有効にするための有効にしたと記憶
		isPlayerOperationRelateOptionOnceActivation_ = true;

		//スタートのSEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/nc237652.wav");
	}
}

//ポーズ時の処理
void Player::PauseProcess()
{
	//ポーズ状態の動機
	isPause_ = dynamic_cast<PlayScene*>(pParent_)->IsPause();

	//マウスを表示/非表示にする
	isMouseCursorDisplayed_ = ShowCursor(signbit((float)isMouseCursorDisplayed_));

	//中央の画像の描画およびそのカウントを有効/無効
	isCentralImageDrawingAndItsCount_ = !isCentralImageDrawingAndItsCount_;

	//プレイヤーの移動を有効/無効
	isPlayerMove_ = !isPlayerMove_;

	//マウス操作(カメラ操作)を有効/無効
	isMouseControl_ = !isMouseControl_;
}

//プレイヤーの移動の処理
void Player::PlayerMoveProcess()
{
	//プレイヤーの移動が有効の場合の処理
	if (isPlayerMove_)
	{
		//プレイヤーが空中にいる場合の処理
		if (isPlayerInTheAir_)
		{
			//現在の移動量に抵抗(0.9倍)を与え、入力移動量を減らす
			movingDistanceByEntry_ = movingDistanceByEntry_ * AIR_RESISTANCE;

			//今フレームの入力移動量を与える(0.32倍)
			movingDistanceByEntry_ += XMVector3Normalize(XMVectorSet(KeyStateCalculation(DIK_D, DIK_A), 0, KeyStateCalculation(DIK_W, DIK_S), 0)) * REDUCED_MOVEMENT_IN_THE_AIR;

			//入力移動量の長さが1を超えた場合正規化し移動量とする処理
			if (XMVector3Length(movingDistanceByEntry_).vecX > XMVector3Length(g_XMIdentityR0).vecX)
				playerMovingDistance_ = XMVector3Normalize(movingDistanceByEntry_);

			//入力移動量の長さが1を超えていない場合そのまま今フレームの移動量とする処理
			else
				playerMovingDistance_ = movingDistanceByEntry_;
		}

		//プレイヤーが地面にいる場合の処理
		else
		{
			//いずれか[X軸移動:Dキー押下・Aキー押下 / Z軸移動:Wキー押下・Sキー押下]を行った場合による入力移動量の取得
			movingDistanceByEntry_ = XMVector3Normalize(XMVectorSet(KeyStateCalculation(DIK_D, DIK_A), 0, KeyStateCalculation(DIK_W, DIK_S), 0));

			//今フレームの移動量に設定
			playerMovingDistance_ = movingDistanceByEntry_;
		}

		//プレイヤーの移動速度の設定処理
		PlayerMoveVelocitySettingProcess();

		//プレイヤーの向きをもとにプレイヤーの移動量を回転
		playerMovingDistance_ = XMVector3TransformCoord(playerMovingDistance_, playerAngleYMatrix_);

		//プレイヤーのY軸移動(ジャンプ)の処理
		PlayerJumpProcess();

		//プレイヤーの移動量による壁へのめり込みを回避させる処理(Y軸->X軸->Z軸の順番で処理)
		ReturnFromTheWallOnTheYAxis();
		ReturnFromTheWallOnTheXorZAxis(AXIS_X);
		ReturnFromTheWallOnTheXorZAxis(AXIS_Z);

		//プレイヤーの移動
		transform_.position_ += playerMovingDistance_;
	}
}

//プレイヤーのY軸移動(ジャンプ)の処理
void Player::PlayerJumpProcess()
{
	//プレイヤーの座標に壁までの距離と重力を与えた場合の座標
	XMVECTOR destinationCoordinates = transform_.position_ + XMVectorSet(WALL_SIDE_HARF, -GRAVITY, WALL_SIDE_HARF, 0);

	//Y軸移動距離を渡す
	playerMovingDistance_.vecY += playerJumpAmount_;

	//プレイヤーが地面に触れていて、空中にいない場合の処理
	if (IS_WALL(destinationCoordinates) && isPlayerInTheAir_ == false)
	{
		//Y軸移動量を0にする
		playerJumpAmount_ = 0;

		//スペースキーを押下している場合の処理
		if (IsKey(DIK_SPACE))
		{
			//プレイヤーのジャンプ量に初速を与える
			playerJumpAmount_ = INITIAL_VELOCITY;

			//空中に浮いた判定にする
			isPlayerInTheAir_ = true;

			//ジャンプSEを流す
			SingleSE::PlaySoundEffect("Assets/Sound/20210420160011.wav");
		}
	}

	//プレイヤーが地面に触れていないまたは空中にいる場合に重力をかける処理
	else
		playerJumpAmount_ -= GRAVITY;

	//プレイヤーのY軸移動のマイナス値が最大(壁までの距離)を上回っていた場合、最大値に変更する処理
	if (playerJumpAmount_ < -WALL_SIDE_HARF)
		playerJumpAmount_ = -WALL_SIDE_HARF;
}

//プレイヤーのY軸方向の移動量による壁へのめり込みを回避させる処理
void Player::ReturnFromTheWallOnTheYAxis()
{
	//1回目:下面の処理 / 2回目:上面の処理
	for (int surface = 0; surface < NUMBER_OF_COUNTER_ELECTRODE_SURFACES; surface++)
	{
		//Y軸の移動先座標
		XMVECTOR destinationCoordinates = transform_.position_;
		destinationCoordinates.vecY += playerMovingDistance_.vecY;

		//下面を判定する場合にはプレイヤーの図上を基として計算するための処理(地面からの距離を考慮)
		if (surface == LEFT_OR_BOTTOM_OR_FRONT)
			destinationCoordinates.vecY += PLAYER_HEIGHT + WALL_SIDE_HARF;

		//壁かどうかを判定するための座標(XZ幅を考慮する)
		XMVECTOR wallIsDetermineForCoordinate = destinationCoordinates + XMVectorSet(WALL_SIDE_HARF, 0, WALL_SIDE_HARF, 0);

		//プレイヤーの移動先の座標が壁の中にある場合の処理
		if (IS_WALL(wallIsDetermineForCoordinate))
		{
			//壁の位置
			int wallPosition = (int)destinationCoordinates.vecY;

			//プレイヤーの移動先の位置(壁までの距離を引き、プレイヤー本体の位置を取る)
			float playerPosition = destinationCoordinates.vecY - WALL_SIDE_HARF;

			//下面の場合のプレイヤーの移動先の位置を決定する処理(壁までの距離を足す)
			if (surface == LEFT_OR_BOTTOM_OR_FRONT)
				playerPosition += WALL_SIDE_HARF;

			//上面の場合のプレイヤーの移動先の位置を決定する処理(壁までの距離を引く)
			else if (surface == RIGHT_OR_TOP_OR_BACK)
				playerPosition -= WALL_SIDE_HARF;

			//めり込んだ分プレイヤーの移動量を戻す
			playerMovingDistance_.vecY += wallPosition - playerPosition;

			//上面の当たり判定の場合に地面に触れている(=空中にいない)判定にする処理
			if (surface == RIGHT_OR_TOP_OR_BACK)
				isPlayerInTheAir_ = false;
		}
	}
}

//プレイヤーのXZ軸方向の移動量による壁へのめり込みを回避させる処理
void Player::ReturnFromTheWallOnTheXorZAxis(int axis)
{
	//1回目->左面、正面の処理
	//2回目->右面、奥面の処理
	for (int surface = 0; surface < NUMBER_OF_COUNTER_ELECTRODE_SURFACES; surface++)
	{
		//移動先座標(対象軸のみ移動先座標として扱う、あらかじめ地面からの距離を考慮)
		XMVECTOR destinationCoordinates = transform_.position_;
		destinationCoordinates.m128_f32[axis] += playerMovingDistance_.m128_f32[axis];
		destinationCoordinates.vecY -= WALL_SIDE_HARF;

		//左面、正面を判定する場合はプレイヤーの幅分当たり判定を大きくする(足す)処理
		if (surface == LEFT_OR_BOTTOM_OR_FRONT)
			destinationCoordinates.m128_f32[axis] += PLAYER_WIDTH;

		//右面、奥面を判定する場合はプレイヤーの幅分当たり判定を大きくする(引く)処理
		else if (surface == RIGHT_OR_TOP_OR_BACK)
			destinationCoordinates.m128_f32[axis] -= PLAYER_WIDTH;

		//壁かどうかを判定するための座標(身長を三分割分)
		XMVECTOR wallIsDetermineForCoordinateLow, wallIsDetermineForCoordinateMiddle, wallIsDetermineForCoordinateHigh;
		wallIsDetermineForCoordinateLow = wallIsDetermineForCoordinateMiddle = wallIsDetermineForCoordinateHigh = destinationCoordinates + XMVectorReplicate(WALL_SIDE_HARF);

		//上記の内中央の判定について、プレイヤーの伸長の半分を足す
		wallIsDetermineForCoordinateMiddle.vecY += PLAYER_HEIGHT_HARF;

		//上記の内一番高い位置の判定について、プレイヤーの伸長分足す(地面からの距離は下判定のため無視)
		wallIsDetermineForCoordinateHigh.vecY += PLAYER_HEIGHT - ADJUSTMENT_PLAYER_HEIGHT + WALL_SIDE_HARF;

		//プレイヤーの移動先の座標が壁の中にある場合の処理
		if (IS_WALL_LOW_OR_MIDDLE_OR_HIGH(wallIsDetermineForCoordinateLow, wallIsDetermineForCoordinateMiddle, wallIsDetermineForCoordinateHigh))
		{
			//壁の位置
			int wallPosition = (int)(destinationCoordinates.m128_f32[axis] + WALL_SIDE_HARF);

			//プレイヤーの移動先の位置
			float playerPosition = destinationCoordinates.m128_f32[axis];

			//左面、正面の場合のプレイヤーの移動先の位置を決定する処理(壁までの距離を足す)
			if (surface == LEFT_OR_BOTTOM_OR_FRONT)
				playerPosition += WALL_SIDE_HARF;

			//右面、奥面の場合のプレイヤーの移動先の位置を決定する処理(壁までの距離を引く)
			else if (surface == RIGHT_OR_TOP_OR_BACK)
 				playerPosition -= WALL_SIDE_HARF;

			//めり込んだ分プレイヤーの移動量を戻す
			playerMovingDistance_.m128_f32[axis] += wallPosition - playerPosition;
		}
	}
}

//プレイヤーの移動速度の設定処理
void Player::PlayerMoveVelocitySettingProcess()
{
	//空中にいない状態で左コントロールキーを押下しているかつ前進をしている場合通常よりも速く動かす状態に変更する処理
	if (!isPlayerInTheAir_ && IsKey(DIK_LCONTROL) && movingDistanceByEntry_.vecZ > 0)
		playerMoveState_ = DASH;

	//空中にいない状態で左シフトキーを押下している場合通常よりも遅く動かす状態に変更する処理
	else if (!isPlayerInTheAir_ && IsKey(DIK_LSHIFT))
		playerMoveState_ = SLOWRY;

	//上記以外の場合通常の速さで動かす状態に変更する処理
	else if (isPlayerInTheAir_ && !IsKey(DIK_LCONTROL) && !IsKey(DIK_LSHIFT))
		playerMoveState_ = NORMAL;

	//速度を設定
	switch (playerMoveState_)
	{
		//通常の速さ
	case NORMAL:
		playerMovingDistance_ *= PLAYER_XZ_NORMAL_MOVE_SPEED;
		break;

		//通常よりも速い
	case DASH:
		playerMovingDistance_ *= PLAYER_XZ_FAST_MOVE_SPEED;
		break;

		//通常よりも遅い
	case SLOWRY:
		playerMovingDistance_ *= PLAYER_XZ_SLOWLY_MOVE_SPEED;
		break;
	}
}

//マウスの操作設定の処理
void Player::MouseControlProcess()
{
	//マウス操作(カメラ操作)が有効の場合の処理
	if (isMouseControl_)
	{
		//ウィンドウの位置取得
		RECT lprc;
		GetWindowRect(Direct3D::hwnd_, &lprc);

		//マウスカーソルの位置取得
		POINT mouceCursorPosition;
		GetCursorPos(&mouceCursorPosition);

		//マウスカーソルをセットする位置
		POINT setMoucePos;

		//x(ウィンドウの中心)
		setMoucePos.x = lprc.left + Direct3D::GetWindowCenterX();

		//y(ウィンドウの中心)
		setMoucePos.y = lprc.top + Direct3D::GetWindowCenterY();

		//マウスカーソルの位置設定
		SetCursorPos(setMoucePos.x, setMoucePos.y);
	}
}

//カメラの移動処理
void Player::CameraMoveProcess()
{
	//マウス操作(カメラ操作)が有効の場合の処理
	if (isMouseControl_)
	{
		//マウス操作によるカメラの位置の操作
		transform_.rotate_.vecY += Input::GetMouseMoveX() * MOUSE_CURSOR_HORIZONTAL_MOVE_SPEED;
		cameraHeight_ += Input::GetMouseMoveY() * MOUSE_CURSOR_VERTICAL_MOVE_SPEED;

		//カメラの位置とターゲットを設定
		XMVECTOR camPos = transform_.position_;
		XMVECTOR camTar = CAMERA_TARGET_POSITION;

		//プレイヤーの向きをもとにカメラのターゲットを回転
		camTar = XMVector3TransformCoord(camTar, playerAngleYMatrix_);

		//カメラを高くする(斜め下を向く)
		camPos.vecY += CAMERA_HEIGHT;
		camTar.vecY -= cameraHeight_;

		//カメラ位置と焦点を設定変更
		Camera::SetPosition(camPos);
		Camera::SetTarget(transform_.position_ + camTar);
	}
}