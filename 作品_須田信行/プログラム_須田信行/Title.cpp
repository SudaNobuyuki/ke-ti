//インクルード
#include "Title.h"
#include "StartScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "SingleSE.h"


//定数
//Initialize()
static const float CLEAR_COLOR[RGB] = { 1, 1, 1 };		//背景色を白

//PressEnterImageProcessSettingAlpha()
static const int PRESS_ENTER_IMAGE_MAX_DRAWING_TIME = 60;		//pressEnterImageDrawTime_の最大値

//TitleBGMPlayProcess()
static const int TITLE_BGM_PLAY_TIME_MAXIMUM = 642;		//titleBGMPlayTime_の最大値


//マクロ
//PressEnterImageSetAlphaProgress()
#define DECIDE_PRESS_ENTER_IMAGE_ALPHA (abs(sin(XMConvertToRadians(pressEnterImageTotalDrawingTime_ * 3.0f))))		//"PressEnterToStart"画像のα値を決める


//コンストラクタ
Title::Title(GameObject * parent) : GameObject(parent, "Title"),
	pressEnterImageID_(INITIAL_ID), pressEnterImageTotalDrawingTime_(0), pressEnterImageAlpha_(0),
	titleImageID_(INITIAL_ID),
	titleBGMPlayTime_(0)
{
}

//デストラクタ
Title::~Title()
{
}

//初期化
void Title::Initialize()
{
	//背景色変更
	Direct3D::SetClearColor(CLEAR_COLOR);

	//"PressEnterToStart"画像の初期設定
	PressEnterImageInitialSetting();

	//タイトル画像の初期設定
	TitleImageInitialSetting();
}

//更新
void Title::Update()
{
	//"PressEnterToStart"画像のα値設定する処理
	PressEnterImageProcessSettingAlpha();

	//タイトルBGM再生処理
	TitleBGMPlayProcess();

	//ステージセレクトシーンへ遷移する処理
	NextScene();
}

//描画
void Title::Draw()
{
	//"PressEnterToStart"画像の描画
	Image::Draw(pressEnterImageID_);

	//タイトル画像の描画
	Image::Draw(titleImageID_ );
}

//開放
void Title::Release()
{
}

//"PressEnterToStart"画像の初期設定
void Title::PressEnterImageInitialSetting()
{
	//"PressEnterToStart"画像データのロード
	pressEnterImageID_ = Image::Load("Assets/StartScene/PressEnterToStart.png");
	assert(pressEnterImageID_ >= 0);

	//タイトル画像の拡大率をウィンドウサイズに調整
	Image::SetWindowSize(pressEnterImageID_);

	//"PressEnterToStart"画像のトランスフォームを設定
	Image::SetTransform(pressEnterImageID_, transform_);
}

//"PressEnterToStart"画像のα値設定する処理
void Title::PressEnterImageProcessSettingAlpha()
{
	//pressEnterImageDrawTime_をインクリメントした際に60を超えた場合に0に戻す処理
	if (++pressEnterImageTotalDrawingTime_ >= PRESS_ENTER_IMAGE_MAX_DRAWING_TIME)
		pressEnterImageTotalDrawingTime_ = 0;

	//"PressEnterToStart"画像のα値を決める
	pressEnterImageAlpha_ = DECIDE_PRESS_ENTER_IMAGE_ALPHA;

	//"PressEnterToStart"画像のα値を渡す
	Image::SetAlpha(pressEnterImageID_, pressEnterImageAlpha_);
}

//タイトル画像の初期設定
void Title::TitleImageInitialSetting()
{
	//タイトル画像データのロード
	titleImageID_ = Image::Load("Assets/StartScene/Title.png");
	assert(titleImageID_ >= 0);

	//タイトル画像の拡大率をウィンドウサイズに調整
	Image::SetWindowSize(titleImageID_);
}

//タイトルBGM再生処理
void Title::TitleBGMPlayProcess()
{
	//タイトルBGM再生時間が0を下回った場合タイトルBGMを流す処理
	if (titleBGMPlayTime_-- <= 0)
	{
		//タイトルBGMを流す
		SingleSE::PlaySoundEffect("Assets/Sound/NKM-G-45-31-63343122-0-11188-63-88-4-2924-44-0-100-140-40-542-13-513-115-308-0-0-453.wav");
		
		//タイトルBGM再生時間を初期化
		titleBGMPlayTime_ = TITLE_BGM_PLAY_TIME_MAXIMUM;
	}
}

//ステージセレクトシーンへ遷移する処理
void Title::NextScene()
{
	//ステージセレクトシーンへ遷移する処理(いずれか[スペースキー押下・エンターキー押下・マウスを左クリック]を行った場合)
	if (Input::KeyStateCheckDown(DIK_SPACE, DIK_RETURN) || Input::IsMouseButtonDown(MOUSE_LEFT))
	{
		//スタートSEを流す
		SingleSE::PlaySoundEffect("Assets/Sound/nc23432.wav");

		//シーンを移動
		dynamic_cast<StartScene*>(pParent_)->NextScene();
	}
}