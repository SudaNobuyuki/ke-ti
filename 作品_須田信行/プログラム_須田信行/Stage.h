//インクルードガード
#pragma once
#ifndef STAGE_H
#define STAGE_H
#endif // !STAGE_H


//インクルード
#include "Engine/GameObject.h"


//マクロ
static const int STAGE_AREA = 50;		//ステージの広さ(50*50*50)


//ステージを管理するクラス
class Stage : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Stage(GameObject* parent);

	//デストラクタ
	//param :なし
	~Stage();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*ステージモデル管理*/

	//ステージのモデルの種類
	enum STAGE_MODELS
	{
		FLOOR,
		WALL,
		GOAL,
		STAGE_MODEL_MAXIMUM
	};

	//ステージモデルID
	int stageModelID_[STAGE_MODEL_MAXIMUM];

	//各ステージのモデルの初期設定
	//param :なし
	//return:なし
	void StageModelInitialSetting();

	//各ステージのモデルを描画させる処理
	//param :なし
	//return:なし
	void StageModelDrawProcess();

private:
	/*背景モデル管理*/

	//背景モデルID
	int backgroundModelID_;

	//背景モデルのトランスフォーム
	Transform backgroundModelTransform_;

	//背景モデルの初期設定
	//param :なし
	//return:なし
	void BackgroundModelInitialSetting();

	//背景モデルを回転させる処理
	//param :なし
	//return:なし
	void BackgroundModelRotationProcess();

private:
	/*ステージを管理*/

	//ステージ高さ
	int stageHeight_;

	//ステージ幅
	int stageWidth_;

	//ステージ奥行
	int stageDepth_;

	//ステージ情報(各座標毎)
	int stageData_[STAGE_AREA][STAGE_AREA][STAGE_AREA];

	//ステージ情報の初期設定
	//param :なし
	//return:なし
	void StageDataInitialSetting();

public:
	/*ステージの情報取得*/

	//そこは壁か調べる
	//param :座標
	//return:壁ならTrue、床ならFalse
	bool IsWall(XMVECTOR& position) { return stageData_[(int)position.vecY][(int)position.vecX][(int)position.vecZ]; }

	//そこがゴールか調べる
	//param :座標
	//return:ゴールならTrue、床ならFalse
	bool IsGoal(XMVECTOR&position) { if (stageData_[(int)position.vecY][(int)position.vecX][(int)position.vecZ] == GOAL)return true; return false; }
};