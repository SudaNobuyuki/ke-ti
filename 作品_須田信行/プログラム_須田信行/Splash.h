//インクルードガード
#pragma once
#ifndef SPLASH_H
#define SPLASH_H
#endif // !SPLASH_H


//インクルード
#include "Engine/GameObject.h"


//スプラッシュ画像を管理するクラス
class Splash : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Splash(GameObject* parent);

	//デストラクタ
	//param :なし
	~Splash();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*スプラッシュ画像管理*/

	//スプラッシュ画像ID
	int splashImageID_;

	//スプラッシュ画像のフェード間隔((0〜125):フェードイン / (126〜249):そのまま表示 / (250〜400):フェードアウト / (401〜):タイトルオブジェクト生成)
	int splashImageFadeInterval_;

	//スプラッシュ画像のα値
	float splashImageAlpha_;

	//スプラッシュ画像の初期設定
	//param :なし
	//return:なし
	void SplashImageInitialSetting();

	//スプラッシュ画像のフェード処理
	//param :なし
	//return:なし
	void SplashImageFadeProcess();

	//タイトルオブジェクトを生成し、自信を殺す処理(splashImageFadeInterval_が401フレーム以降の)
	//param :なし
	//return:なし
	void TitleObjectInstantiateProcess();
};