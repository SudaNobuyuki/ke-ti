//インクルード
#include <wincodec.h>
#include "Texture.h"
#include "Direct3D.h"


//コメント
#pragma comment( lib, "WindowsCodecs.lib" )


//コンストラクタ
Texture::Texture():
	pSampler_(nullptr), pShaderResourceView_(nullptr), width_(0), height_(0)
{
}

//デストラクタ
Texture::~Texture()
{
}

//画像の読み込み
HRESULT Texture::Load(std::string fileName)
{
	//初期化
	IWICImagingFactory* pFactory = nullptr;
	IWICBitmapDecoder* pDecoder = nullptr;
	IWICBitmapFrameDecode* pFrame = nullptr;
	IWICFormatConverter* pFormatConverter = nullptr;

	//ファイル名をワイド文字へ変換
	wchar_t wchar[FILENAME_MAX];
	size_t size;
	mbstowcs_s(&size, wchar, fileName.c_str(), fileName.length());

	//画像ファイルロード
	CoInitialize(nullptr);

	//コンポーネントオブジェクトの生成
	CHECK_HRESULT(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void**>(&pFactory)), "コンポートオブジェクトの生成に失敗しました");

	//画像の読み込み
	CHECK_HRESULT(pFactory->CreateDecoderFromFilename(wchar, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder), "画像の読み込みに失敗しました");

	//フレームの取得
	CHECK_HRESULT(pDecoder->GetFrame(0, &pFrame), "フレームの取得に失敗しました");

	//コンバータのフォーマットの生成
	CHECK_HRESULT(pFactory->CreateFormatConverter(&pFormatConverter), "コンバータのフォーマットの生成に失敗しました");

	//コンバータのフォーマットの初期化
	CHECK_HRESULT(pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut), "コンバータのフォーマットの初期化に失敗しました");

	CoUninitialize();

	//画像サイズを調べる
	CHECK_HRESULT(pFormatConverter->GetSize(&width_, &height_), "画像サイズの取得に失敗しました");

	//テクスチャを作成
	ID3D11Texture2D* pTexture;
	D3D11_TEXTURE2D_DESC texture2DDesc;
	texture2DDesc.Width = width_;
	texture2DDesc.Height = height_;
	texture2DDesc.MipLevels = 1;
	texture2DDesc.ArraySize = 1;
	texture2DDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texture2DDesc.SampleDesc.Count = 1;
	texture2DDesc.SampleDesc.Quality = 0;
	texture2DDesc.Usage = D3D11_USAGE_DYNAMIC;
	texture2DDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texture2DDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texture2DDesc.MiscFlags = 0;
	CHECK_HRESULT(Direct3D::pDevice_->CreateTexture2D(&texture2DDesc, nullptr, &pTexture), "2Dテクスチャの作成に失敗しました");

	//テクスチャをコンテキストに渡す
	D3D11_MAPPED_SUBRESOURCE mapped;
	CHECK_HRESULT(Direct3D::pContext_->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped), "テクスチャをコンテキストに渡すことに失敗しました");
	CHECK_HRESULT(pFormatConverter->CopyPixels(nullptr, width_ * 4, width_ * height_ * 4, (BYTE*)mapped.pData), "ピクセルデータのコピーに失敗しました");
	Direct3D::pContext_->Unmap(pTexture, 0);

	//サンプラー作成
	D3D11_SAMPLER_DESC sampler;
	ZeroMemory(&sampler, sizeof(D3D11_SAMPLER_DESC));
	sampler.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampler.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampler.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampler.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	CHECK_HRESULT(Direct3D::pDevice_->CreateSamplerState(&sampler, &pSampler_), "サンプラーの作成に失敗しました");

	//シェーダーリソースビューの作成
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceView = {};
	shaderResourceView.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	shaderResourceView.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceView.Texture2D.MipLevels = 1;
	CHECK_HRESULT(Direct3D::pDevice_->CreateShaderResourceView(pTexture, &shaderResourceView, &pShaderResourceView_), "シェーダーリソースビューの作成");

	//解放
	SAFE_RELEASE(pTexture);
	SAFE_RELEASE(pFormatConverter);
	SAFE_RELEASE(pFrame);
	SAFE_RELEASE(pDecoder);
	SAFE_RELEASE(pFactory);

	//成功
	return S_OK;
}

//Textureの解放
void Texture::Release()
{
	SAFE_RELEASE(pShaderResourceView_);
	SAFE_RELEASE(pSampler_);
}