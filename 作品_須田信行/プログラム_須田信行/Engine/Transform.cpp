//インクルード
#include "Transform.h"


//コンストラクタ
Transform::Transform():
	movingMatrix_(XMMatrixIdentity()), rotationMatrix_(XMMatrixIdentity()), scaleMatrix_(XMMatrixIdentity()),
	position_(g_XMZero), rotate_(g_XMZero), scale_(g_XMOne3), pParent_(nullptr)
{
}

//デストラクタ
Transform::~Transform()
{
}

//ワールド行列を取得
XMMATRIX Transform::GetWorldMatrix()
{
	//親がいない場合は自身のみ返す
	if (pParent_ == nullptr)
		return scaleMatrix_ * rotationMatrix_ * movingMatrix_;

	//自身のワールド座標に親のワールド座標をかける
	return scaleMatrix_ * rotationMatrix_ * movingMatrix_ * pParent_->GetWorldMatrix();
}

//各行列の計算
void Transform::Calclation()
{
	//移動行列
	movingMatrix_ = XMMatrixTranslation(position_.vecX, position_.vecY, position_.vecZ);

	//回転行列
	XMMATRIX xMatrix, yMatrix, zMatrix;
	xMatrix = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));
	yMatrix = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));
	zMatrix = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));
	rotationMatrix_ = zMatrix * xMatrix * yMatrix;

	//拡大行列
	scaleMatrix_ = XMMatrixScaling(scale_.vecX, scale_.vecY, scale_.vecZ);
}