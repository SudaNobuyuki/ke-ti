//インクルードガード
#pragma once
#ifndef MODEL_H
#define MODEL_H
#endif //!MODEL_H


//インクルード
#include <vector>
#include "Fbx.h"


//モデルを管理する名前空間
namespace Model
{
	//1モデルの情報
	struct ModelData
	{
		Transform transform_;		//トランスフォーム
		std::string filePath_;		//モデルパス
		Fbx* pFbx_;					//モデル
	};

	//モデルをロード
	//引数:読み込むモデルのパス
	//戻値:正常にモデルを読み込めたら0以上、読み込めなかったら-1
	int Load(std::string modelPath);

	//モデルの表示設定
	//引数:表示させるモデルの番号
	//引数:表示させる位置
	//戻値:なし
	void SetTransform(int modelHandle, Transform& transform);

	//モデルを描画
	//引数:表示させるモデルの番号
	//戻値:なし
	void Draw(int modelHandle);

	//モデルすべて解放
	//引数:なし
	//戻値:なし
	void Release();
}