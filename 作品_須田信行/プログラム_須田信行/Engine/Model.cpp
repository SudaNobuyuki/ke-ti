//インクルード
#include "Model.h"


//モデルを管理する名前空間
namespace Model
{
	//モデルを格納するベクター
	std::vector <ModelData*> datas_;
}

//モデルをロード
int Model::Load(std::string modelPath)
{
	//構造体をインスタンス化
	ModelData* pModelData = new ModelData;

	//モデルパスを格納
	pModelData->filePath_ = modelPath;

	//すでに同じモデルデータが読み込まれているかチェック
	for (int i = 0; i < (int)datas_.size(); i++)
	{
		//すでに同じモデルデータが読み込まれていた場合
		if (datas_[i]->filePath_ == pModelData->filePath_)
		{
			//Fbxオブジェクトを共有
			pModelData->pFbx_ = datas_[i]->pFbx_;

			//動的配列に上記モデル構造体を格納
			datas_.push_back(pModelData);

			//モデル番号(配列の要素数 - 1)を返す
			return (int)datas_.size() - 1;
		}
	}

	//Fbxオブジェクトを作成
	pModelData->pFbx_ = new Fbx;

	//モデルパスに格納されたモデルデータのロードし、成功した場合
	if (pModelData->pFbx_->Load(pModelData->filePath_) == MB_OK)
	{
		//動的配列に上記モデル構造体を格納
		datas_.push_back(pModelData);

		//モデル番号(配列の要素数 - 1)を返す
		return (int)datas_.size() - 1;
	}

	//モデルの読み込みに失敗した場合には-1を返す
	return -1;
}

//モデルの表示設定
void Model::SetTransform(int modelHandle, Transform& transform)
{
	//-1は何もしない
	if (modelHandle < 0)return;

	//トランスフォームを計算
	transform.Calclation();

	//モデルの表示位置指定
	datas_[modelHandle]->transform_ = transform;
}

//モデルを描画
void Model::Draw(int modelHandle)
{
	//-1は何もしない
	if (modelHandle < 0)return;

	//モデルを描画
	datas_[modelHandle]->pFbx_->Draw(datas_[modelHandle]->transform_);
}

//モデルすべて解放
void Model::Release()
{
	//全モデルが対象
	for (int i = 0; i < (int)datas_.size(); i++)
	{
		//自身が削除済みか
		bool flg = true;

		//自身(と同じ名前のファイルパスのオブジェクト)が消されているか
		for (int j = 0; j < i; j++)
		{
			//自身と同じやつがいる
			if (datas_[i]->filePath_ == datas_[j]->filePath_)
			{
				//消した
				flg = false;
				break;
			}
		}

		//自身が解放されていないとき解放
		if (datas_[i]->pFbx_ && flg)
		{
			datas_[i]->pFbx_->Release();
			SAFE_DELETE(datas_[i]->pFbx_);
		}
	}

	//全モデルが対象
	for (int i = 0; i < (int)datas_.size(); i++)
	{
		//Spriteの解放
		SAFE_DELETE(datas_[i]);
	}

	//ベクタークリア
	datas_.clear();
}