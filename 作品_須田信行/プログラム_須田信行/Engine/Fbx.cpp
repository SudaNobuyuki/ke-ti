﻿//インクルード
#include "Fbx.h"
#include "Direct3D.h"
#include "Camera.h"
#include "Texture.h"
#include <string>


//コンストラクタ
Fbx::Fbx() :
	vertexCount_(0), polygonCount_(0),materialCount_(0), pIndexCountEachMaterial_(0), ppIndex_(0),
	pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pConstantBuffer_(nullptr), pMaterialList_(nullptr), pVertices_(nullptr)
{
}

//デストラクタ
Fbx::~Fbx()
{
}

//読み込み
HRESULT Fbx::Load(std::string fileName)
{
	//マネージャを生成
	FbxManager* pFbxManager = FbxManager::Create();

	//インポーターを生成
	FbxImporter* fbxImporter = FbxImporter::Create(pFbxManager, "imp");
	fbxImporter->Initialize(fileName.c_str(), -1, pFbxManager->GetIOSettings());

	//シーンオブジェクトにFBXファイルの情報を流し込む
	FbxScene* pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	fbxImporter->Import(pFbxScene);
	fbxImporter->Destroy();


	//メッシュ情報を取得
	FbxNode* rootNode = pFbxScene->GetRootNode();		//ルートノードの取得
	FbxNode* pNode = rootNode->GetChild(0);				//ルートノードの第一子を取得
	FbxMesh* mesh = pNode->GetMesh();					//第一子のメッシュ情報取得

	//各情報の個数を取得
	vertexCount_ = mesh->GetControlPointsCount();		//頂点の数
	polygonCount_ = mesh->GetPolygonCount();			//ポリゴンの数
	materialCount_ = pNode->GetMaterialCount();			//ノードからポリゴン数取得

	//現在のカレントディレクトリを覚えておく
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//引数のfileNameからディレクトリ部分を取得
	char dir[MAX_PATH];
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);

	//カレントディレクトリ変更
	SetCurrentDirectory(dir);

	//頂点バッファ準備
	CHECK_HRESULT(InitializeVertex(mesh), "FBXのLoadに失敗しました");

	//インデックスバッファ準備
	CHECK_HRESULT(InitializeIndex(mesh), "FBXのLoadに失敗しました");

	//コンスタントバッファ準備
	CHECK_HRESULT(InitializeConstantBuffer(), "FBXのLoadに失敗しました");

	//マテリアル準備
	CHECK_HRESULT(InitializeMaterial(pNode), "FBXのLoadに失敗しました");

	//カレントディレクトリを元に戻す
	SetCurrentDirectory(defaultCurrentDir);

	//マネージャ解放
	pFbxManager->Destroy();

	//成功
	return S_OK;
}

//頂点バッファ準備
HRESULT Fbx::InitializeVertex(fbxsdk::FbxMesh* mesh)
{
	//頂点情報を入れる配列
	pVertices_ = new VERTEX[vertexCount_];

	//全ポリゴン
	for (DWORD poly = 0; poly < (unsigned)polygonCount_; poly++)
	{
		//3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//調べる頂点の番号
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置
			FbxVector4 pos = mesh->GetControlPointAt(index);
			pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);

			//頂点のUV
			FbxVector2 uv = mesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0 - uv.mData[1]), 0, 0);
		}
	}

	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA subresourceData;
	subresourceData.pSysMem = pVertices_;

	//頂点バッファの生成
	CHECK_HRESULT(Direct3D::pDevice_->CreateBuffer(&bufferDesc, &subresourceData, &pVertexBuffer_), "頂点バッファの作成に失敗しました");
	
	//成功
	return S_OK;
}

//インデックスバッファ準備
HRESULT Fbx::InitializeIndex(fbxsdk::FbxMesh* mesh)
{
	//マテリアル数
	ppIndexBuffer_ = new ID3D11Buffer*[materialCount_];

	//頂点数格納用
	pIndexCountEachMaterial_ = new int[materialCount_];

	//インデックス数
	ppIndex_ = new int*[materialCount_];

	//マテリアル分
	for (int i = 0; i < materialCount_; i++)
	{
		//インデックス回数
		ppIndex_[i] = new int[polygonCount_ * 3];
		int count = 0;

		//全ポリゴン
		for (DWORD poly = 0; poly < (unsigned)polygonCount_; poly++)
		{
			//どのマテリアルなのか調べる
			FbxLayerElementMaterial * material = mesh->GetLayer(0)->GetMaterials();
			int materialID = material->GetIndexArray().GetAt(poly);

			//調べたマテリアルの番号が今回しているマテリアル回数と一致した場合
			if (materialID == i)
			{
				//3頂点分
				for (int vertex = 2; vertex >= 0; vertex--)
				{
					ppIndex_[i][count] = mesh->GetPolygonVertex(poly, vertex);
					count++;
				}
			}
		}
		pIndexCountEachMaterial_[i] = count;

		// インデックスバッファを生成する
		D3D11_BUFFER_DESC bufferDesc;
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.ByteWidth = sizeof(int) * count;
		bufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
		bufferDesc.CPUAccessFlags = 0;
		bufferDesc.MiscFlags = 0;

		//インデックスバッファの作成
		D3D11_SUBRESOURCE_DATA subresourceData;
		subresourceData.pSysMem = ppIndex_[i];
		subresourceData.SysMemPitch = 0;
		subresourceData.SysMemSlicePitch = 0;
		CHECK_HRESULT(Direct3D::pDevice_->CreateBuffer(&bufferDesc, &subresourceData, &ppIndexBuffer_[i]), "インデックスバッファの作成に失敗しました");
	}

	//成功
	return S_OK;
}

//コンスタントバッファ準備
HRESULT Fbx::InitializeConstantBuffer()
{
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;

	// コンスタントバッファの作成
	CHECK_HRESULT(Direct3D::pDevice_->CreateBuffer(&bufferDesc, nullptr, &pConstantBuffer_), "コンスタントバッファバッファの作成に失敗しました");
	
	//成功
	return S_OK;
}

//マテリアル準備
HRESULT Fbx::InitializeMaterial(fbxsdk::FbxNode* pNode)
{
	//マテリアル情報を入れる配列
	pMaterialList_ = new MATERIAL[materialCount_];

	//マテリアル情報格納
	for (int i = 0; i < materialCount_; i++)
	{
		//i番目のマテリアル情報を取得
		FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);

		//マテリアルの色
		FbxSurfacePhong* pMaterialPhong = (FbxSurfacePhong*)pNode->GetMaterial(i);
		FbxDouble3 diffuse = pMaterialPhong->Diffuse;
		pMaterialList_[i].diffuse = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);

		//テクスチャ情報
		FbxProperty lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);

		//テクスチャの数数
		int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();

		//テクスチャあり
		if (fileTextureCount)
		{
			//テクスチャの詳細
			FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);

			//テクスチャパス取得
			const char* textureFilePath = textureInfo->GetRelativeFileName();
			
			//ファイル名+拡張だけにする
			char name[_MAX_FNAME];		//ファイル名
			char ext[_MAX_EXT];			//拡張子
			_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
			wsprintf(name, "%s%s", name, ext);

			//ファイルからテクスチャ作成
			pMaterialList_[i].pTexture = new Texture;
			pMaterialList_[i].pTexture->Load(name);
		}

		//テクスチャ無し
		else
		{
			//テクスチャ指定しない
			pMaterialList_[i].pTexture = nullptr;
		}

	}
	
	//成功
	return S_OK;
}

//描画
HRESULT Fbx::Draw(Transform & transform)
{
	//使用するシェーダーのセットを指定する
	Direct3D::SetShaderBundle(SHADER_3D);
	
	//頂点バッファ
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	Direct3D::pContext_->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	//使用するコンスタントバッファをシェーダに伝える
	Direct3D::pContext_->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
	Direct3D::pContext_->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

	//ここからはマテリアル毎の処理
	for (int i = 0; i < materialCount_; i++)
	{
		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext_->IASetIndexBuffer(ppIndexBuffer_[i], DXGI_FORMAT_R32_UINT, 0);

		//コンスタントバッファに渡す情報
		D3D11_MAPPED_SUBRESOURCE data;
		CONSTANT_BUFFER constantBuffer;
		constantBuffer.WVPMatrix = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());	//ワールド・ビュー・プロジェクションの合成行列（頂点の変形に使用）
		constantBuffer.diffuseColor = pMaterialList_[i].diffuse;
		constantBuffer.isTexture = false;		//テクスチャなしで初期化

		//テクスチャありの場合
		if (pMaterialList_[i].pTexture)
		{
			//サンプラー（テクスチャの表示の仕方）を設定
			ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();
			Direct3D::pContext_->PSSetSamplers(0, 1, &pSampler);

			//シェーダーリソースビュー（テクスチャ）を設定
			ID3D11ShaderResourceView* pShaderResourceView = pMaterialList_[i].pTexture->GetSRV();
			Direct3D::pContext_->PSSetShaderResources(0, 1, &pShaderResourceView);

			//テクスチャ使ってるかフラグをTRUEにする
			constantBuffer.isTexture = TRUE;
		}

		//テクスチャ無しの場合
		else
		{
			//テクスチャ使ってるかフラグをFALSEにする
			constantBuffer.isTexture = FALSE;
		}

		//コンスタントバッファに情報をコピー
		CHECK_HRESULT(Direct3D::pContext_->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &data), "GPUからのアクセスを止めることに失敗しました");
		memcpy_s(data.pData, data.RowPitch, (void*)(&constantBuffer), sizeof(constantBuffer));					// データを値を送る
		Direct3D::pContext_->Unmap(pConstantBuffer_, 0);														//再開

		//描画
		Direct3D::pContext_->DrawIndexed(pIndexCountEachMaterial_[i], 0, 0);
	}
	
	//成功
	return S_OK;
}

//解放
void Fbx::Release()
{
	//マテリアル分
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_ARRAY_DELETE(ppIndex_[i]);
	}
	SAFE_ARRAY_DELETE(pVertices_);
	//マテリアル分
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(pMaterialList_[i].pTexture);
	}
	SAFE_ARRAY_DELETE(pMaterialList_);
	SAFE_RELEASE(pConstantBuffer_);
	//マテリアル分
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
	}
	SAFE_ARRAY_DELETE(ppIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}