﻿//インクルード
#include "Camera.h"


//カメラを管理する名前空間
namespace Camera
{
	//場所と行列
	XMVECTOR position_;			//カメラの位置(視点)
	XMVECTOR target_;			//見る位置(焦点)
	XMMATRIX viewMatrix_;		//ビュー行列
	XMMATRIX projMatrix_;		//プロジェクション行列
}

//初期化
void Camera::Initialize()
{
	//場所の初期化
	position_ = XMVectorSet(0, 3, -10, 0);	//カメラの位置
	target_ = XMVectorSet(0, 0, 0, 0);		//カメラの焦点

	//プロジェクション行列
	projMatrix_ = XMMatrixPerspectiveFovLH(1, (FLOAT)800 / (FLOAT)600, 0.1f, 100.0f);
}

//更新
void Camera::Update()
{
	//ビュー行列の作成
	viewMatrix_ = XMMatrixLookAtLH(position_, target_, XMVectorSet(0, 1, 0, 0));
}

//位置を設定
void Camera::SetPosition(float x, float y, float z)
{
	//位置の設定
	SetPosition(XMVectorSet(x, y, z, 0));
}

//位置を設定
void Camera::SetPosition(const XMVECTOR& position)
{
	//位置の設定
	position_ = position;
}

//焦点を設定
void Camera::SetTarget(float x, float y, float z)
{
	//焦点を設定
	SetTarget(XMVectorSet(x, y, z, 0));
}

//焦点を設定
void Camera::SetTarget(const XMVECTOR& target)
{
	//焦点を設定
	target_ = target;
}

//ビュー行列を取得
XMMATRIX Camera::GetViewMatrix()
{
	//ビュー行列を取得
	return viewMatrix_;
}

//プロジェクション行列を取得
XMMATRIX Camera::GetProjectionMatrix()
{
	//プロジェクション行列を取得
	return projMatrix_;
}