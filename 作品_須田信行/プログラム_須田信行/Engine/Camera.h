﻿//インクルードガード
#pragma once
#ifndef CAMERA_H
#define CAMERA_H
#endif //!CAMERA_H


//インクルード
#include <DirectXMath.h>
#include "Direct3D.h"


//名前空間
using namespace DirectX;


//カメラを管理する名前空間
namespace Camera
{
	//場所
	extern XMVECTOR position_;	//カメラの位置(視点)
	extern XMVECTOR target_;	//見る位置(焦点)

	//カメラの初期化(プロジェクション行列作成)
	//引数:なし
	//戻値:なし
	void Initialize();

	//カメラの更新(ビュー行列作成)
	//引数:なし
	//戻値:なし
	void Update();

	//視点(カメラの位置)を設定
	//引数:位置(カメラの場所)
	//戻値:なし
	void SetPosition(float x, float y, float z);
	void SetPosition(const XMVECTOR& position);

	//焦点(見る位置)を設定
	//引数:焦点(カメラが見る点)
	//戻値:なし
	void SetTarget(float x, float y, float z);
	void SetTarget(const XMVECTOR& target);

	//ビュー行列を取得
	//引数:なし
	//戻値:ビュー行列(XMMATRIX)
	XMMATRIX GetViewMatrix();

	//プロジェクション行列を取得
	//引数:なし
	//戻値:プロジェクション行列(射影行列)(XMMATRIX)
	XMMATRIX GetProjectionMatrix();
};