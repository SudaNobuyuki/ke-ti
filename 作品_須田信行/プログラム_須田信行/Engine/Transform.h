//インクルードガード
#pragma once
#ifndef TRANSFORM_H
#define TRANSFORM_H
#endif //!TRANSFORM_H


//インクルード
#include <DirectXMath.h>


//マクロ
#define vecX m128_f32[0]		//XMVECTOR型の成分x
#define vecY m128_f32[1]		//XMVECTOR型の成分y
#define vecZ m128_f32[2]		//XMVECTOR型の成分z


//名前空間
using namespace DirectX;


//オブジェクトの位置、向き、拡大率などを管理するクラス
class Transform
{
public:
	/*コンストラクタ、デストラクタ*/

	//コンストラクタ
	Transform();

	//デストラクタ
	~Transform();

public:
	/*行列*/

	//移動行列
	XMMATRIX movingMatrix_;

	//回転行列
	XMMATRIX rotationMatrix_;

	//拡大行列
	XMMATRIX scaleMatrix_;

	//ワールド行列を取得
	//引数:なし
	//戻値:ワールド行列(拡大行列*  回転行列*  移動行列)
	XMMATRIX GetWorldMatrix();

public:
	/*トランスフォームの情報*/

	//位置
	XMVECTOR position_;

	//向き
	XMVECTOR rotate_;

	//拡大率
	XMVECTOR scale_;

	//親のトランスフォーム
	Transform* pParent_;

	//各行列の計算
	//引数:なし
	//戻値:なし
	void Calclation();
};