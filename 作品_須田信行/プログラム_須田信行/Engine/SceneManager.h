//インクルードガード
#pragma once
#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H
#endif //!SCENEMANAGER_H


//インクルード
#include "GameObject.h"


//シーン
enum SCENE_ID
{
	SCENE_ID_START = 0,
	SCENE_ID_SELECT,
	SCENE_ID_PLAY,
	SCENE_MAX
};


//シーンマネージャーを管理するクラス
class SceneManager : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//引数:なし
	SceneManager();
	
	//デストラクタ
	//引数:なし
	~SceneManager();

	//初期化
	//引数:なし
	//戻値:なし
	void Initialize() override;
	
	//更新
	//引数:なし
	//戻値:なし
	void Update() override;
	
	//描画
	//引数:なし
	//戻値:なし
	void Draw() override;
	
	//解放
	//引数:なし
	//戻値:なし
	void Release() override;

private:
	/*シーンの管理*/

	//現在のシーン
	SCENE_ID currentSceneID_;

	//次のシーン
	SCENE_ID nextSceneID_;

public:
	/*シーン遷移*/

	//特定シーンへの切り替え
	//引数:シーン名
	//戻値:なし
	void ChangeScene(SCENE_ID nextSceneID) { nextSceneID_ = nextSceneID; }

	//次のシーンへ移動(存在しない場合には一番最初のシーンへ戻る)
	//引数:なし
	//戻値:なし
	void NextScene();

	//自身のシーンへ移動
	//引数:なし
	//戻値:なし
	void ReScene() { currentSceneID_ = SCENE_MAX; }

public:
	/*初回プレイを記憶*/

	//初回プレイフラグ(0:まだ / 1:初回プレイ / 2:初回プレイ済み)
	int isFirstPlay_;
};