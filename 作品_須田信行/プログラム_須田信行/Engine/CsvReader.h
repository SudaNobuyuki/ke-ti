//インクルードガード
#pragma once
#ifndef CSV_H
#define CSV_H
#endif //!CSV_H


//インクルード
#include <vector>
#include <string>


//CSVファイルを管理するクラス
class CsvReader
{
public:
	/*コンストラクタ、デストラクタ*/

	//コンストラクタ
	CsvReader();

	//デストラクタ
	~CsvReader();

private:
	/*csvの管理*/

	//読み込んだデータを入れておく
	std::vector<std::vector<std::string>> data_;

	//「,」か「改行」までの文字列を取得
	//引数:結果を入れるアドレス
	//引数:もとの文字列データ
	//引数:何文字目から調べるか
	//戻値:なし
	void GetToComma(std::string* result, std::string data, DWORD* index);

public:
	/*データの取得*/

	//CSVファイルのロード
	//引数:ファイル名
	//戻値:成功/失敗
	bool Load(std::string fileName);

	//指定した位置のデータを文字列で取得
	//引数:取得したい位置
	//戻値:取得した文字列
	std::string GetString(DWORD x, DWORD y);

	//指定した位置のデータを整数で取得
	//引数:取得したい位置
	//戻値:取得した値
	int GetValue(DWORD x, DWORD y) { return atoi(GetString(x, y).c_str()); }

	//ファイルの行列数を取得
	//引数:なし
	//戻値:行列数
	size_t GetWidth() { return data_[0].size(); }
	size_t GetHeight() { return data_.size(); }
};