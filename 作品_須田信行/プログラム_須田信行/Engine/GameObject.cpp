//インクルード
#include "GameObject.h"


//コンストラクタ(引数なし)
GameObject::GameObject():GameObject(nullptr, "")
{
}

//コンストラクタ(引数あり)
GameObject::GameObject(GameObject* parent, const std::string& name) :
	pParent_(parent), objectName_(name), isDead_(false)
{
	//子供リストの初期化
	childList_.clear();
}

//デストラクタ
GameObject::~GameObject()
{
	//自身の子供をすべて殺す関数
	AllChildKill();

	//親を解放
	pParent_ = nullptr;
}

/*********************
　基本関数のサブ関数
**********************/

//更新補助関数
void GameObject::UpdateSub()
{
	//自分のUpdate関数を呼ぶ
	Update();

	//すべての位置(親子ともども)を変えたい
	transform_.Calclation();

	//すべての子供のUpdateSubを呼ぶ
	for (auto it = childList_.begin(); it != childList_.end(); it++)
		(*it)->UpdateSub();

	//死に判定を呼ぶ
	for (auto it = childList_.begin(); it != childList_.end(); )
	{
		//死に判定あり
		if ((*it)->isDead_)
		{
			//解放
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}

		//死に判定なし
		else
			it++;
	}
}

//描画補助関数
void GameObject::DrawSub()
{
	//自分のDraw関数を呼ぶ
	Draw();

	//すべての子供のDrawSubを呼ぶ
	for (auto it = childList_.begin(); it != childList_.end(); it++)
		(*it)->DrawSub();
}

//解放補助
void GameObject::ReleaseSub()
{
	//自分のRelease関数を呼ぶ
	Release();

	//すべての子供のReleaseSubを呼ぶ
	for (auto it = childList_.begin(); it != childList_.end(); it++)
		(*it)->ReleaseSub();
}

/*********************
　オブジェクトを殺す
**********************/

//自身の子供をすべて殺す関数
void GameObject::AllChildKill()
{
	//すべての子供のReleaseを呼ぶ
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->Release();
		delete(*itr);
	}

	//子供リストの開放
	childList_.clear();
}