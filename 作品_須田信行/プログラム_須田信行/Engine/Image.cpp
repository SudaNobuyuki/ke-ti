//インクルード
#include "Image.h"


//画像を管理する名前空間
namespace Image
{
	//画像を格納するベクター
	std::vector <ImageData*> datas_;

	//画像を描画しているときの奥行(MAX100枚)
	float depth_ = 0.01f;

	//画像を手前へ重ねる
	float approach_ = 0.0001f;
}

//画像をロード
int Image::Load(std::string ImagePath)
{
	//構造体をインスタンス化
	ImageData* pImageData = new ImageData;

	//画像パスを格納
	pImageData->filePath_ = ImagePath;

	//すでに同じ画像データが読み込まれているかチェック
	for (int i = 0; i < (int)datas_.size(); i++)
	{
		//すでに同じ画像データが読み込まれていた場合
		if (datas_[i]->filePath_ == pImageData->filePath_)
		{
			//Spriteオブジェクトを共有
			pImageData->pSprite_ = datas_[i]->pSprite_;

			//動的配列に上記画像構造体を格納
			datas_.push_back(pImageData);

			//画像番号(配列の要素数 - 1)を返す
			return (int)datas_.size() - 1;
		}
	}

	//Spriteオブジェクトを作成
	pImageData->pSprite_ = new Sprite;

	//画像パスに格納された画像データのロードし、成功した場合
	if (pImageData->pSprite_->Initialize(pImageData->filePath_) == MB_OK)
	{
		//動的配列に上記画像構造体を格納
		datas_.push_back(pImageData);

		//画像番号(配列の要素数 - 1)を返す
		return (int)datas_.size() - 1;
	}

	//画像の読み込みに失敗した場合には-1を返す
	return -1;
}

//画像の表示位置のみ設定
void Image::SetPosition(int imageHandle, Transform& transform)
{
	//-1は何もしない
	if (imageHandle < 0)return;

	//座標を設定
	datas_[imageHandle]->transform_.position_ = transform.position_;
}

//画像の表示位置と拡大率設定
void Image::SetPositionAndScale(int imageHandle, Transform& transform)
{
	//-1は何もしない
	if (imageHandle < 0)return;

	//座標を設定
	datas_[imageHandle]->transform_.position_ = transform.position_;

	//拡大率を設定
	datas_[imageHandle]->transform_.scale_ = transform.scale_;
}

//画像の表示設定
void Image::SetTransform(int imageHandle, Transform& transform)
{
	//-1は何もしない
	if (imageHandle < 0)return;

	//画像の表示位置指定
	datas_[imageHandle]->transform_ = transform;
}

//画像を描画
void Image::Draw(int imageHandle)
{
	//-1は何もしない
	if (imageHandle < 0)return;

	//奥行セット
	datas_[imageHandle]->transform_.position_.vecZ = depth_;

	//現在の奥行を再計算
	depth_ -= approach_;

	//画像を描画
	datas_[imageHandle]->pSprite_->Draw(datas_[imageHandle]->transform_);
}

//画像すべて解放
void Image::Release()
{
	//全画像が対象
	for (int i = 0; i < (int)datas_.size(); i++)
	{
		//自身が削除済みか
		bool flg = true;

		//自身(と同じ名前のファイルパスのオブジェクト)が消されているか
		for (int j = 0; j < i; j++)
		{
			//自身と同じやつがいる
			if (datas_[i]->filePath_ == datas_[j]->filePath_)
			{
				//消した
				flg = false;
				break;
			}
		}

		//自身が解放されていないとき解放
		if (datas_[i]->pSprite_ && flg)
		{
			datas_[i]->pSprite_->Release();
			SAFE_DELETE(datas_[i]->pSprite_);
		}
	}

	//全画像が対象
	for (int i = 0; i < (int)datas_.size(); i++)
	{
		//Spriteの解放
		SAFE_DELETE(datas_[i]);
	}

	//ベクタークリア
	datas_.clear();
}

//α値セット
void Image::SetAlpha(int imageHandle, float a)
{
	//-1は何もしない
	if (imageHandle < 0)return;

	//α値セット
	datas_[imageHandle]->pSprite_->SetAlpha(a);
}

//α値リセット
void Image::ResetAlpha(int imageHandle)
{
	//-1は何もしない
	if (imageHandle < 0)return;

	//α値リセット
	SetAlpha(imageHandle, 1.0f);
}

//画像の拡大率をウィンドウサイズに調整
XMVECTOR Image::SetWindowSize(int imageHandle, float scale)
{
	//-1は何もしない
	if (imageHandle < 0)return g_XMZero;

	//画像サイズを一度元に戻す
	datas_[imageHandle]->transform_.scale_ = g_XMOne;

	//ウィンドウサイズに調整
	datas_[imageHandle]->transform_.scale_.vecX *= (float)Direct3D::screenWidth_ / datas_[imageHandle]->pSprite_->GetSizeX();
	datas_[imageHandle]->transform_.scale_.vecY *= (float)Direct3D::screenHeight_ / datas_[imageHandle]->pSprite_->GetSizeY();

	//最終的な倍率に調整
	return datas_[imageHandle]->transform_.scale_ *= scale;
}

//画像の奥行をリセット
void Image::ResetDepth()
{
	//奥行リセット
	depth_ = 0.01f;
}
