//インクルードガード
#pragma once
#ifndef GAMEOBJECT
#define GAMEOBJECT
#endif // !GAMEOBJECT


//インクルード
#include <list>
#include <vector>
#include <string>
#include "Transform.h"
#include "Direct3D.h"


//マクロ
#define INITIAL_ID -1		//モデルや画像の初期値


//ゲームオブジェクト
class GameObject
{
public:
	/***********
	　基本ゲームオブジェクト関数
	************/

	//コンストラクタ(引数なし)
	//引数:なし
	//戻値:なし
	GameObject();

	//コンストラクタ(引数あり)
	//引数:親ゲームオブジェクト
	//引数:ゲームオブジェクト名
	//戻値:なし
	GameObject(GameObject* parent, const std::string& name);

	 //デストラクタ
	 //引数:なし
	 //戻値:なし
	virtual ~GameObject();

	//初期化(純粋仮想関数)
	virtual void Initialize() = 0;

	//更新(純粋仮想関数)
	virtual void Update() = 0;

	//描画(純粋仮想関数)
	virtual void Draw() = 0;

	//解放(純粋仮想関数)
	virtual void Release() = 0;

	/*********************
	  基本関数のサブ関数
	**********************/

	//更新補助関数
	//引数:なし
	//戻値:なし
	void UpdateSub();

	//描画補助関数
	//引数:なし
	//戻値:なし
	void DrawSub();

	//解放補助
	//引数:なし
	//戻値:なし
	void ReleaseSub();

	/*********************
	   オブジェクトを殺す
	**********************/

	//自身を殺す準備をする関数
	//引数:殺すかどうか
	//戻値:なし
	void KillMe(bool kill = true) { if (this)isDead_ = kill; }

	//自身の子供をすべて殺す関数
	//引数:なし
	//戻値:なし
	void AllChildKill();

	/*******************
	  オブジェクト探査
	********************/

	//親オブジェクトを取得
	//引数:なし
	//戻値:親オブジェクトのアドレス
	GameObject* GetParent() { return pParent_; };

	/*********************
	  オブジェクトを追加
	**********************/

	//オブジェクトを子供として追加するテンプレート関数
	//引数:なし
	//戻値:作られたオブジェクトのポインタ
	template<class T>
	GameObject* Instantiate()
	{
		//オブジェクトの作成
		T* p = new T(this);

		//リストへの子供の追加
		this->childList_.push_back(p);

		//オブジェクトの初期化
		p->Initialize();

		//親のトランスフォームを通知
		p->transform_.pParent_ = &this->transform_;

		//作られたオブジェクトのポインタを返す
		return p;
	}

private:
	/*自身の状況*/

	//オブジェクトネーム記憶
	std::string objectName_;

	//自身が死んでいいかどうか記憶
	bool isDead_;

protected:
	/*関連ゲームオブジェクト*/

	//子供のゲームオブジェクト格納
	std::list<GameObject*> childList_;

	//親のゲームオブジェクト記憶
	GameObject* pParent_;

public:
	/*自身のトランスフォーム*/

	//トランスフォーム
	Transform transform_;
};