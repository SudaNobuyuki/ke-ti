//インクルードガード
#pragma once
#ifndef STAGECONFIG_H
#define STAGECONFIG_H
#endif // !STAGECONFIG_H


//インクルード
#include <string>


//ステージコンフィグを管理する名前空間
namespace StageConfig
{
	//セレクトステージ
	enum STAGES
	{
		STAGE_1,
		STAGE_2,
		STAGE_3,
		STAGE_MAXIMUM
	};

	//現在選択しているステージ
	extern STAGES selectStage_;

	//ステージ画像のパスの取得
	//param :取得したいステージ番号
	//return:ステージ画像のパス
	std::string GetStageImagePath(int stage);

	//ステージ説明画像のパスの取得
	//param :取得したいステージ番号
	//return:ステージ説明画像のパス
	std::string GetStageDescriptionImagePath(int stage);

	//ステージデータのパスの取得
	//param :なし
	//return:ステージデータのパス
	std::string GetStageDataPath();

	//ステージデータ設定のパスの取得
	//param :なし
	//return:ステージデータ設定のパス
	std::string GetStageDataConfigPath();

	//ステージ記録のパスの取得
	//param :なし
	//return:テージ記録のパス
	std::string GetStageRecordPath();
}