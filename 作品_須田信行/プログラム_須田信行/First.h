//インクルードガード
#pragma once
#ifndef FIRST_H
#define FIRST_H
#endif // !FIRST_H


//インクルード
#include "Engine/GameObject.h"


//時間を管理するクラス
class First : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	First(GameObject* parent);

	//デストラクタ
	//param :なし
	~First();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*操作方法画像管理*/

	//操作方法画像の種類
	enum HOW_TO_PLAY_IMAGES
	{
		CONTROL_1,
		CONTROL_2,
		GOAL_1,
		GOAL_2,
		HOW_TO_PLAY_IMAGE_MAXIMUM
	};

	//操作方法画像ID
	int howToPlayImageID_[HOW_TO_PLAY_IMAGE_MAXIMUM];
	
	//現在表示している画像
	int drawImage_;

	//表示している画像の表示時間
	int drawTime_;

	//各操作方法画像の初期設定
	//param :なし
	//return:なし
	void HowToPlayImageInitialSetting();
};