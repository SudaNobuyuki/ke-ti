//インクルードガード
#pragma once
#ifndef SELECTOR_H
#define SELECTOR_H
#endif // !SELECTOR_H


//インクルード
#include "Engine/GameObject.h"
#include "StageConfig.h"


//ステージセレクト(操作)を管理するクラス
class Selector : public GameObject
{
public:
	/*基本ゲームオブジェクト関数*/

	//コンストラクタ
	//param :親ゲームオブジェクト
	//param :ゲームオブジェクト名
	Selector(GameObject* parent);

	//デストラクタ
	//param :なし
	~Selector();

	//初期化
	//param :なし
	//return:なし
	void Initialize() override;

	//更新
	//param :なし
	//return:なし
	void Update() override;

	//描画
	//param :なし
	//return:なし
	void Draw() override;

	//開放
	//param :なし
	//return:なし
	void Release() override;

private:
	/*ステージ画像管理*/

	//各ステージ画像ID
	int stageImageID_[StageConfig::STAGE_MAXIMUM];

	//ステージ画像の拡大率をウィンドウサイズに合わせた際の大きさを記憶
	XMVECTOR stageImageScale_;

	//各ステージ画像の初期設定
	//param :なし
	//return:なし
	void StageImageInitialSetting();

	//各ステージ画像の描画座標と拡大率を設定する処理
	//param :なし
	//return:なし
	void StageImageSetPositionAndScale();

private:
	/*ステージの説明画像管理*/

	//各ステージの説明画像ID
	int stageDescriptionImageID_[StageConfig::STAGE_MAXIMUM];

	//各ステージの説明画像の初期設定
	//param :なし
	//return:なし
	void StageDescriptionImageInitialSetting();

private:
	/*ステージの種類管理*/

	//現在選択しているステージ
	int selectStage_;

	//選択しているステージを変更した際に前のステージを記憶
	int previousSelectStage_;

	//選択しているステージを変更する際にどちらへ移動させているか
	int selectStageMoveDirection_;

	//選択しているステージを変更する際に見た目で移動している様に処理するための処理時間(50フレーム分)
	int selectStageMoveTime_;

	//選択しているステージを変更する際に見た目で移動している様に処理する際のX座標ポジション(50フレームで1ずらす)
	float selectStageMovePosition_;

	//選択しているステージの拡縮周期
	int selectStageContractionCycle_;

	//選択しているステージを変更する処理
	//param :なし
	//return:なし
	void SelectStageChangeProcess();

private:
	/*プレイシーンへ遷移する関数管理*/

	//プレイシーンへ遷移する処理
	//param :なし
	//return:なし
	void NextScene();
};