//インクルード
#include "SingleSE.h"
#include <assert.h>
#include <fstream>


//音を鳴らす
void SingleSE::PlaySoundEffect(LPCSTR soundPath, DWORD flag)
{
	//ファイルが存在しない場合強制終了
	std::ifstream ifs(soundPath);
	assert(ifs);

	//決められた音を待ちなしで実行
	PlaySound(soundPath, nullptr, flag);
}