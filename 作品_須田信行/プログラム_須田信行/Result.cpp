//インクルード
#include "Result.h"
#include "PlayScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "SingleSE.h"


//定数
//ResultImageProcessSettingAlpha()
static const int TIME_START_DRAWING = 240;				//"今回の記録"描画開始タイミング
static const int RECORD_START_DRAWING = 300;			//"過去最高記録"描画開始タイミング
static const int NEW_RECORD_START_DRAWING = 360;		//"New Record!!"描画開始タイミング


//マクロ
//ResultImageProcessSettingAlpha()
#define DECIDE_NEW_RECORD_IMAGE_ALPHA (abs(sinf(XMConvertToRadians((resultImageDrawTime_ - 360) * 2.0f))))		//"New Record!!"画像のα値を決める


//コンストラクタ
Result::Result(GameObject * parent)
	:GameObject(parent, "Result"),
	resultImageDrawTime_(0)
{
}

//デストラクタ
Result::~Result()
{
}

//初期化
void Result::Initialize()
{
	//各リザルト画像の初期設定
	ResultImageInitialSetting();
}

//更新
void Result::Update()
{
	//ゴールしている場合各リザルト画像のアルファ値を設定する処理
	if(dynamic_cast<PlayScene*>(pParent_)->IsGoal())
		ResultImageProcessSettingAlpha();

	//今回の時間と過去最高記録時間が描画された時にいずれか[スペースキー押下・エンターキー押下・マウスを左クリック]を行った場合にステージセレクトシーンへ遷移する処理
	if (resultImageDrawTime_ > RECORD_START_DRAWING && (Input::KeyStateCheckDown(DIK_SPACE, DIK_RETURN) || Input::IsMouseButtonDown(MOUSE_LEFT)))
		dynamic_cast<PlayScene*>(pParent_)->ChangeScene();
}

//描画
void Result::Draw()
{
	//各リザルト画像の描画
	for (int i = 0; i < RESULT_IMAGE_MAXIMUM; i++)
		Image::Draw(resultImageID_[i]);
}

//開放
void Result::Release()
{
}

//各リザルト画像の初期設定
void Result::ResultImageInitialSetting()
{
	std::string resultImagePath[RESULT_IMAGE_MAXIMUM] = 
	{
		"Assets/Result/Result.png",
		"Assets/Result/Time.png",
		"Assets/Result/Record.png",
		"Assets/Result/NewRecord.png",
	};

	//各リザルト画像のロードおよび拡大率をウィンドウサイズに調整し、アルファ値を0に設定
	for (int i = 0; i < RESULT_IMAGE_MAXIMUM; i++)
	{
		//リザルトデータのロード
		resultImageID_[i] = Image::Load(resultImagePath[i]);
		assert(resultImageID_[i] >= 0);

		//リザルト画像の拡大率をウィンドウサイズに調整する
		Image::SetWindowSize(resultImageID_[i]);

		//リザルト画像のアルファ値を0にする
		Image::SetAlpha(resultImageID_[i], 0);
	}
}

//各リザルト画像のアルファ値を設定する
void Result::ResultImageProcessSettingAlpha()
{
	//リザルト画像の描画時間を増やす
	resultImageDrawTime_++;

	//背景画像のアルファ値を設定
	Image::SetAlpha(resultImageID_[BACKGROUND], resultImageDrawTime_ / 180.0f);

	//リザルト画像の描画時間が240になった場合クリアSEを流す処理
	if (resultImageDrawTime_ == TIME_START_DRAWING)
		SingleSE::PlaySoundEffect("Assets/Sound/nc131422.wav");

	//リザルト画像の描画時間が240を超えた場合、"今回の記録"を表示させる処理
	if (resultImageDrawTime_ > TIME_START_DRAWING)
		Image::ResetAlpha(resultImageID_[TIME]);

	//リザルト画像の描画時間が300を超えた場合、今回の時間、"過去最高記録"、過去最高の時間を表示させる処理
	if (resultImageDrawTime_ > RECORD_START_DRAWING)
		Image::ResetAlpha(resultImageID_[RECORD]);

	//リザルト画像の描画時間が360を超えて過去最高記録を上回った場合、"New Record!!"を点滅させる処理
	if (resultImageDrawTime_ > NEW_RECORD_START_DRAWING && dynamic_cast<PlayScene*>(pParent_)->IsNewRecord())
		Image::SetAlpha(resultImageID_[NEW_RECORD], DECIDE_NEW_RECORD_IMAGE_ALPHA);
}