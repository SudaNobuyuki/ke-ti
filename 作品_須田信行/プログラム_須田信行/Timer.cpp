//インクルード
#include "Timer.h"
#include <fstream>
#include "PlayScene.h"
#include "StageConfig.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//定数
//コンストラクタ
static const int INITIAL_TOTAL_TIME = -1;		//総時間の初期値

//Update()
static const int TOTAL_TIME_MAXIMUM = 35940;		//総時間の最大値

//TimeImageInitialSetting()
static const float TIME_IMAGE_SCALE = 0.8f;		//各数字の最終的な拡大率

//TimeImageDrawProcess()
static const XMVECTOR NORMAL_SECOND_DISPLAY_POSITION = XMVectorSet(0.8f, -0.8f, 0, 0);		//通常時の秒の表示位置
static const int RECORD_START_DRAWING = 300;												//記録の描画開始タイミング
static const XMVECTOR RESULT_SECOND_DISPLAY_POSITION = XMVectorSet(0.2f, -0.1f, 0, 0);		//リザルト時の秒の表示位置
static const XMVECTOR RECORD_SECOND_DISPLAY_POSITION = XMVectorSet(0.2f, -0.7f, 0, 0);		//リザルト時の過去最高記録の秒の表示位置
static const float BETWEEN_STRINGS_AND_NUMBERS = 0.125f;									//文字と数字の間
static const float BETWEEN_NUMBERS_AND_NUMBERS = 0.1f;										//数字と数字の間


//マクロ
//TimeImageDrawProcess()
#define FIRST_PLACE_OF_SECONDS(p) (p / 60 % 10)			//秒の1の位
#define TENTH_PLACE_OF_SECONDS(p) (p / 60 / 10 % 6)		//秒の10の位
#define MINUTES(p) (p / 3600)							//分

//IsNewRecord(), GoalProcess()
#define ROUND_DOWN (totalTime_ / 60 * 60)		//総時間の端数を切る


//コンストラクタ
Timer::Timer(GameObject * parent) : GameObject(parent, "Timer"),
	totalTime_(INITIAL_TOTAL_TIME), startRecordDrawTime_(0), recordTime_(0)
{
	//各数字画像の初期化
	for (int i = 0; i < TIME_IMAGE_MAXIMUM; i++)
		timeImageID_[i] = INITIAL_ID;
}

//デストラクタ
Timer::~Timer()
{
}

//初期化
void Timer::Initialize()
{
	//各時間画像の初期設定
	TimeImageInitialSetting();

	//過去最高記録時間の取得
	GetRecordTime();
}

//更新
void Timer::Update()
{
	//ポーズ中でなくゴールしていないかつ総時間が経常最大値を下回っていた場合に総時間を増やす処理
	if (!dynamic_cast<PlayScene*>(pParent_)->IsPauseOrGoal() && totalTime_ < TOTAL_TIME_MAXIMUM)
		totalTime_++;
}

//描画
void Timer::Draw()
{
	//時間の描画処理
	TimeImageDrawProcess();

	//ゴールしている場合記録の描画開始タイミングを計る
	if (dynamic_cast<PlayScene*>(pParent_)->IsGoal())
		startRecordDrawTime_++;
}

//開放
void Timer::Release()
{
}

//各時間画像の初期設定
void Timer::TimeImageInitialSetting()
{
	//各時間画像データのパス
	std::string timeImagePath[TIME_IMAGE_MAXIMUM] =
	{
		"Assets/Numbers/0.png",
		"Assets/Numbers/1.png",
		"Assets/Numbers/2.png",
		"Assets/Numbers/3.png",
		"Assets/Numbers/4.png",
		"Assets/Numbers/5.png",
		"Assets/Numbers/6.png",
		"Assets/Numbers/7.png",
		"Assets/Numbers/8.png",
		"Assets/Numbers/9.png",
		"Assets/Numbers/Minute.png",
		"Assets/Numbers/Second.png",
	};

	//各時間画像のロードおよび拡大率をウィンドウサイズに調整
	for (int i = 0; i < TIME_IMAGE_MAXIMUM; i++)
	{
		//時間画像データのロード
		timeImageID_[i] = Image::Load(timeImagePath[i]);
		assert(timeImageID_[i] >= 0);

		//時間画像の拡大率をウィンドウサイズに調整し、0.8倍する
		Image::SetWindowSize(timeImageID_[i], TIME_IMAGE_SCALE);
	}
}

//時間の描画処理
void Timer::TimeImageDrawProcess()
{
	//ポーズ中でなくゴールしていない場合の処理
	if (!dynamic_cast<PlayScene*>(pParent_)->IsPauseOrGoal())
	{
		//"秒"の表示位置
		transform_.position_ = NORMAL_SECOND_DISPLAY_POSITION;
	
		//総時間の描画
		TimeDraw(totalTime_);
	}

	//リザルト時の時間表示中の場合の処理
	else if(dynamic_cast<PlayScene*>(pParent_)->IsGoal() && startRecordDrawTime_ > RECORD_START_DRAWING)
	{
		//時間を記録する場合は記録する
		SaveTime();

		//"秒"の表示位置
		transform_.position_ = RESULT_SECOND_DISPLAY_POSITION;

		//総時間の描画
		TimeDraw(totalTime_);

		//"秒"の表示位置
		transform_.position_ = RECORD_SECOND_DISPLAY_POSITION;

		//総時間の描画
		TimeDraw(recordTime_);
	}
}

//時間の描画
void Timer::TimeDraw(int time)
{
	//"秒"
	Image::SetPosition(timeImageID_[SECOND], transform_);
	Image::Draw(timeImageID_[SECOND]);

	//秒の1の位
	transform_.position_.vecX -= BETWEEN_STRINGS_AND_NUMBERS;
	Image::SetPosition(timeImageID_[FIRST_PLACE_OF_SECONDS(time)], transform_);
	Image::Draw(timeImageID_[FIRST_PLACE_OF_SECONDS(time)]);

	//秒の10の位
	transform_.position_.vecX -= BETWEEN_NUMBERS_AND_NUMBERS;
	Image::SetPosition(timeImageID_[TENTH_PLACE_OF_SECONDS(time)], transform_);
	Image::Draw(timeImageID_[TENTH_PLACE_OF_SECONDS(time)]);

	//"分"
	transform_.position_.vecX -= BETWEEN_STRINGS_AND_NUMBERS;
	Image::SetPosition(timeImageID_[MINUTE], transform_);
	Image::Draw(timeImageID_[MINUTE]);

	//分
	transform_.position_.vecX -= BETWEEN_STRINGS_AND_NUMBERS;
	Image::SetPosition(timeImageID_[MINUTES(time)], transform_);
	Image::Draw(timeImageID_[MINUTES(time)]);
}

//過去最高記録時間の取得
void Timer::GetRecordTime()
{
	//タイム取得
	std::ifstream iFile(StageConfig::GetStageRecordPath());

	//ファイルから時間を取得
	std::string time;
	getline(iFile, time);

	//文字列から数値に変換
	recordTime_ = std::stoi(time);

	//ファイルを閉じる
	iFile.close();
}

//時間を記録する場合は記録する
void Timer::SaveTime()
{
	//過去最高記録時間を更新している場合の処理
	if (IsNewRecord())
	{
		//ファイルに書き込む
		std::ofstream oFile(StageConfig::GetStageRecordPath());

		//記述
		oFile << ROUND_DOWN;

		//ファイル終了
		oFile.close();
	}
}

//NewRecordか
bool Timer::IsNewRecord()
{
	//NewRecordか確認
	return signbit((float)ROUND_DOWN - recordTime_);
}